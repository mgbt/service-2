<?php

use Illuminate\ Database\ Seeder;

class ProductTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public
    function run() {
        DB::table('products_category') -> insert(
            [
                ['name' => 'ឡេលាបមុខ'], // 1
                ['name' => 'ឡេលាបក្លៀក'], // 2
                ['name' => 'ជែលលាងមុខ'], // 3
                ['name' => 'សេរ៉ូម'],
                ['name' => 'សាប៊ូកក់សក់'],
                ['name' => 'សាប៊ូអប់សក់'],
                ['name' => 'ប្រេងអប់សក់'],
                ['name' => 'សាប៊ូលាងខ្លួន'],
                ['name' => 'ឡូឆាន់'],
                ['name' => 'ទឹកអនាម័យស្ដ្រី'],
                ['name' => 'ម្សៅ'],
            ]
        );

        // ============================================================ Product
        // DB::table('product')->insert([
        //     [
        //         'category_id' => 1,
        //         'sku' => 'TB1901CR',
        //         'kh_name' => 'ឡេសារ៉ាយសមុទ្រ',
        //         'en_name' => 'Pearl Cream',
        //         'image' => 'public/product/1.jpg',
        //     ],
        //     [
        //         'category_id' => 1,
        //         'sku' => 'TB1902CR',
        //         'kh_name' => 'ដេគ្រីម',
        //         'en_name' => 'Product',
        //         'image' => 'public/product/2.jpg',
        //     ],
        //     [
        //         'category_id' => 2,
        //         'sku' => 'TB1903CR',
        //         'kh_name' => 'អាន់ឌើអាមគ្រីម',
        //         'en_name' => 'Arm Cream',
        //         'image' => 'public/product/3.jpg',
        //     ],
        //     [
        //         'category_id' => 3,
        //         'sku' => 'TB1904CR',
        //         'kh_name' => 'ធីទ្រីអយ',
        //         'en_name' => 'Cleansing Gel',
        //         'image' => 'public/product/4.jpg',
        //     ],
        //     [
        //         'category_id' => 3,
        //         'sku' => 'TB1905CG',
        //         'kh_name' => 'ហាន់នី គ្លិនហ្ស៊ិងជែល',
        //         'en_name' => 'Honey Cleansing Gel',
        //         'image' => 'public/product/5.jpg',
        //     ],
        //     [
        //         'category_id' => 3,
        //         'sku' => 'TB1906CG',
        //         'kh_name' => 'អាន់ឌើហ្វមលាងមុខ ព្យាបាលមុខស្ងួត មុខរមាស់ និងអាឡែកស៊ីអាមគ្រីម',
        //         'en_name' => 'អាន់ឌើហ្វមលាងមុខ',
        //         'image' => 'public/product/6.jpg',
        //     ],
        //     [
        //         'category_id' => 2,
        //         'sku' => 'TB1907WS',
        //         'kh_name' => 'វ៉ាយ បឌីម៉ាសក៍',
        //         'en_name' => 'White Body Mask',
        //         'image' => 'public/product/7.jpg',
        //     ],
        //     [
        //         'category_id' => 4,
        //         'sku' => 'TB1908WS',
        //         'kh_name' => 'សេរ៉ូម ២៤ ខេហ្គោល',
        //         'en_name' => '24K Gold Whitening Serum',
        //         'image' => 'public/product/8.jpg',
        //     ],
        //     [
        //         'category_id' => 5,
        //         'sku' => 'TB1909WS',
        //         'kh_name' => 'សាប៊ូកក់សក់ អាហ្គាន អយ (ប្រេងអាហ្គាន) ',
        //         'en_name' => ' Argan Oill Shampoo',
        //         'image' => 'public/product/9.jpg',
        //     ],
        //     [
        //         'category_id' => 2,
        //         'sku' => 'TB1910SP',
        //         'kh_name' => 'សាប៊ូអប់សក់ អាហ្គាន អយ (ប្រេងអាហ្គាន) ',
        //         'en_name' => 'Argan Oil​l Conditioner',
        //         'image' => 'public/product/10.jpg',
        //     ],
        //     [
        //         'category_id' => 2,
        //         'sku' => 'TB1911HC',
        //         'kh_name' => 'ប្រេងអប់សក់ ម៉ូរ៉ូកូ អាហ្គាន អយ  ',
        //         'en_name' => 'Morocco Argan Oill',
        //         'image' => 'public/product/11.jpg',
        //     ],
        //     [
        //         'category_id' => 2,
        //         'sku' => 'TB1903CR',
        //         'kh_name' => 'អាឡូ វេរ៉ា វាយសិន នីង (សាប៊ូប្រទាលកន្ទុយក្រពើ)',
        //         'en_name' => ' Aloe Vera Whitening',
        //         'image' => 'public/product/12.jpg',
        //     ],
        //     [
        //         'category_id' => 2,
        //         'sku' => 'TB1913BL',
        //         'kh_name' => 'គ្លុយតា វាយបឌី ឡូឆាន់ (ឡូឆាន់ គ្លុយតា)',
        //         'en_name' => ' Gluta White Body Lotion',
        //         'image' => 'public/product/13.jpg',
        //     ],
        //     [
        //         'category_id' => 2,
        //         'sku' => 'TB1914WC',
        //         'kh_name' => 'គ្លិនហ្ស៊ិង វ៉តធើ ',
        //         'en_name' => ' Cleansing Water',
        //         'image' => 'public/product/14.jpg',
        //     ],
        //     [
        //         'category_id' => 2,
        //         'sku' => 'TB19145PD',
        //         'kh_name' => 'ម៉ាសសារ៉ាយវេទមន្ដ',
        //         'en_name' => 'ម៉ាសសារ៉ាយវេទមន្ដ',
        //         'image' => 'public/product/15.jpg',
        //     ]

        // ]);

        // ============================================================ Package for signle product
    //     DB::table('package') -> insert([
    //         [
    //             'kh_name' => 'ឡេសារ៉ាយសមុទ្រ',
    //             'en_name' => 'Pearl Cream',
    //             'cost_price' => '4',
    //             'selling_price' => '10',
    //             'image' => 'public/product/1.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ], 
    //         [
    //             'kh_name' => 'ដេគ្រីម',
    //             'en_name' => 'Product',
    //             'cost_price' => '8',
    //             'selling_price' => '10',
    //             'image' => 'public/product/2.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ], 
    //         [
    //             'kh_name' => 'អាន់ឌើអាមគ្រីម',
    //             'en_name' => 'Arm Cream',
    //             'cost_price' => '4',
    //             'selling_price' => '12',
    //             'image' => 'public/product/3.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'ធីទ្រីអយ',
    //             'en_name' => 'Cleansing Gel',
    //             'cost_price' => '2.4',
    //             'selling_price' => '6',
    //             'image' => 'public/product/4.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'ហាន់នី គ្លិនហ្ស៊ិងជែល',
    //             'en_name' => 'Honey Cleansing Gel',
    //             'cost_price' => '6',
    //             'selling_price' => '2.40',
    //             'image' => 'public/product/5.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'អាន់ឌើហ្វមលាងមុខ ព្យាបាលមុខស្ងួត មុខរមាស់ និងអាឡែកស៊ីអាមគ្រីម',
    //             'en_name' => 'អាន់ឌើហ្វមលាងមុខ',
    //             'cost_price' => '4',
    //             'selling_price' => '12',
    //             'image' => 'public/product/6.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'វ៉ាយ បឌីម៉ាសក៍',
    //             'en_name' => 'White Body Mask',
    //             'cost_price' => '2.4',
    //             'selling_price' => '6',
    //             'image' => 'public/product/7.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'សេរ៉ូម ២៤ ខេហ្គោល',
    //             'en_name' => '24K Gold Whitening Serum',
    //             'cost_price' => '2.4',
    //             'selling_price' => '6',
    //             'image' => 'public/product/8.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],

    //         [
    //             'kh_name' => 'សាប៊ូកក់សក់ អាហ្គាន អយ (ប្រេងអាហ្គាន) ',
    //             'en_name' => ' Argan Oill Shampoo',
    //             'cost_price' => '6',
    //             'selling_price' => '10',
    //             'image' => 'public/product/9.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'សាប៊ូអប់សក់ អាហ្គាន អយ (ប្រេងអាហ្គាន) ',
    //             'en_name' => 'Argan Oil​l Conditioner',
    //             'cost_price' => '6',
    //             'selling_price' => '10',
    //             'image' => 'public/product/10.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'ប្រេងអប់សក់ ម៉ូរ៉ូកូ អាហ្គាន អយ  ',
    //             'en_name' => 'Morocco Argan Oill',
    //             'cost_price' => '4',
    //             'selling_price' => '12',
    //             'image' => 'public/product/11.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'អាឡូ វេរ៉ា វាយសិន នីង (សាប៊ូប្រទាលកន្ទុយក្រពើ)',
    //             'en_name' => ' Aloe Vera Whitening',
    //             'cost_price' => '4',
    //             'selling_price' => '12',
    //             'image' => 'public/product/12.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'គ្លុយតា វាយបឌី ឡូឆាន់ (ឡូឆាន់ គ្លុយតា)',
    //             'en_name' => ' Gluta White Body Lotion',
    //             'cost_price' => '4',
    //             'selling_price' => '12',
    //             'image' => 'public/product/13.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'គ្លិនហ្ស៊ិង វ៉តធើ ',
    //             'en_name' => ' Cleansing Water',
    //             'cost_price' => '4',
    //             'selling_price' => '12',
    //             'image' => 'public/product/14.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ],
    //         [
    //             'kh_name' => 'ម៉ាសសារ៉ាយវេទមន្ដ',
    //             'en_name' => 'ម៉ាសសារ៉ាយវេទមន្ដ',
    //             'cost_price' => '4',
    //             'selling_price' => '12',
    //             'image' => 'public/product/15.jpg',
    //             'is_single_product' => 1,
    //             'n_of_units' => 1
    //         ]


    //    ]);
        

        // DB::table('packages_products') -> insert([
        //     [
        //         'package_id' => 1,
        //         'product_id' => 1
        //     ],
        //     [
        //         'package_id' => 2,
        //         'product_id' => 2
        //     ],
        //     [
            
        //         'package_id' => 3,
        //         'product_id' => 3
        //     ],
        //     [
            
        //         'package_id' => 4,
        //         'product_id' => 4
        //     ],
        //     [
            
        //         'package_id' => 5,
        //         'product_id' => 5
        //     ],
        //     [
            
        //         'package_id' => 6,
        //         'product_id' => 6
        //     ],
        //     [
            
        //         'package_id' => 7,
        //         'product_id' => 7
        //     ],
        //     [
            
        //         'package_id' => 8,
        //         'product_id' => 8
        //     ],
        //     [
            
        //         'package_id' => 9,
        //         'product_id' => 9
        //     ],
        //     [
            
        //         'package_id' => 10,
        //         'product_id' => 10
        //     ],
        //     [
            
        //         'package_id' => 11,
        //         'product_id' => 11
        //     ],
        //     [
            
        //         'package_id' => 12,
        //         'product_id' => 12
        //     ],
        //     [
            
        //         'package_id' => 13,
        //         'product_id' => 13
        //     ],
        //     [
            
        //         'package_id' => 14,
        //         'product_id' => 14
        //     ],
        //     [
            
        //         'package_id' => 15,
        //         'product_id' => 15
        //     ]

        // ]);

        // Package With Sigle Product
        // $packageId = DB::table('package')->insertGetId([
        //     'sku' => 'TB1911HC',
        //     'kh_name' => 'ឡេលាបឈុតតូច', 
        //     'en_name' => 'ឡេលាបឈុតតូច', 
        //     'cost_price' => '4',
        //     'selling_price' => '10',
        //     'image' => 'public/product/1.jpg',
        //     'is_single_product' => 0,
        //     'n_of_units' => 2
        // ]); 

        // DB::table('packages_products') -> insert([
        //     [
        //         'package_id' => $packageId,
        //         'product_id' => 1
        //     ],
        //     [
        //         'package_id' => $packageId,
        //         'product_id' => 2
        //     ]
        // ]);


         // Price Ranges
        // for($productId = 1; $productId <= 15; $productId++){
        //     DB::table('product_prices') -> insert(
        //         [
        //             [
        //                 'product_id' => $productId,
        //                 'from' => 1,
        //                 'to' => 9,
        //                 'price' => 13
        //             ],
        //             [
        //                 'product_id' => $productId,
        //                 'from' => 10,
        //                 'to' => 59,
        //                 'price' => 12
        //             ],
        //             [
        //                 'product_id' => $productId,
        //                 'from' => 60,
        //                 'to' => 99,
        //                 'price' => 11
        //             ],
        //             [
        //                 'product_id' => $productId,
        //                 'from' => 100,
        //                 'to' => 249,
        //                 'price' => 10
        //             ],
        //             [
        //                 'product_id' => $productId,
        //                 'from' => 250,
        //                 'to' => 499,
        //                 'price' => 7
        //             ],
        //             [
        //                 'product_id' => $productId,
        //                 'from' => 500,
        //                 'to' => 1000000,
        //                 'price' => 7
        //             ]
        //         ]
        //     );
        // }

        // for($packageId = 1; $packageId <= 16; $packageId++){
        //     DB::table('package_prices') -> insert(
        //         [
        //             [
        //                 'package_id' => $packageId,
        //                 'from' => 1,
        //                 'to' => 9,
        //                 'price' => 13
        //             ],
        //             [
        //                 'package_id' => $packageId,
        //                 'from' => 10,
        //                 'to' => 59,
        //                 'price' => 12
        //             ],
        //             [
        //                 'package_id' => $packageId,
        //                 'from' => 60,
        //                 'to' => 99,
        //                 'price' => 11
        //             ],
        //             [
        //                 'package_id' => $packageId,
        //                 'from' => 100,
        //                 'to' => 249,
        //                 'price' => 10
        //             ],
        //             [
        //                 'package_id' => $packageId,
        //                 'from' => 250,
        //                 'to' => 499,
        //                 'price' => 8.5
        //             ],
        //             [
        //                 'package_id' => $packageId,
        //                 'from' => 500,
        //                 'to' => 1000000,
        //                 'price' => 8.5
        //             ]
        //         ]
        //     );
        // }




       

       
    }
}