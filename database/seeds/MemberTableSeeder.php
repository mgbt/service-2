<?php

use Illuminate\Database\Seeder;
use App\MGTB\Account; 

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
        for($i = 1; $i <= 1; $i++){

            // ============================================================ Branch
            $userId = DB::table('user')->insertGetId([ 
                'type_id'           =>  3, 
                'uid'               =>  Account::generateUid(), 
                'email'             =>  'branch-'.$i.'@mgtb.com',                   
                'phone'             =>  '02000000'.$i, 
                'password'          =>  bcrypt('123456'), 
                'is_active'         =>  1, 
                'is_email_verified' =>  1, 
                'is_phone_verified' =>  1,
                'name_kh' => 'បូ ពិសី',
                'name' => 'Bo Pisey', 
               
               
            ]);

            $memberAsBranchId = DB::table('member')->insertGetId([ 
                'sponsor_id'   => $i-1, 
                'user_id'       => $userId, 
                'province_id'   => $i, 
                'district_id'   => rand(1, 5), 
                //'role_id'       => 4,  //Branch
                //'subscription_id'   => 6, //VVIP
                //'branch_id'   => $i, 
            ]);

            // DB::table('member_subscriptions')->insertGetId([ 
            //     'member_id'         => $memberAsBranchId,
            //     'subscription_id'   => 6, //VVIP
            //     'price'             => 8.5, 
            //     'status_id'         => 2, 
            //     'activated_at'      => now(), 
            //     'expired_at'        => date('Y-m-d 23:59:59', strtotime('+30 days', strtotime(Date('Y-m-d')))), 
            //     'status_updater_id'      => 1
            // ]);


            // ============================================== Add Depot
            // for($j = 1; $j <= 5; $j++){

            //     $userId = DB::table('user')->insertGetId([ 
            //         'type_id'           =>   3, 
            //         'uid'               =>  Account::generateUid(), 
            //         'email'             =>  'depot-'.$i.$j.'@mgtb.com',                   
            //         'phone'             =>  '03000000'.$i.$j, 
            //         'password'          =>  bcrypt('123456'), 
            //         'is_active'         =>  1, 
            //         'is_email_verified' =>  1, 
            //         'is_phone_verified' =>  1,
            //         'name_kh' => 'ដេប៉ូ'.$i.$j, 
            //         'name' => 'Depot'.$i.$j, 
            //         'creator_id' =>  $userId
            //     ]);

            //     $districtId = rand(1, 23); 
            //     $memberAsDepotId = DB::table('member')->insertGetId([ 
            //         'user_id'       => $userId, 
            //         'sponsor_id'    => $memberAsBranchId,
            //         'province_id'   => $i, 
            //         'district_id'   => rand(1, 23), 
            //         //'role_id'       => 4,  //Depot
            //         'subscription_id'   => 5,  //VIP
            //         'depot_id'      => $districtId, 
            //         'branch_owner_id' => $memberAsBranchId
            //     ]);

            //     DB::table('member_subscriptions')->insertGetId([ 
            //         'member_id'         => $memberAsDepotId,
            //         'subscription_id'   => 5, //VIP
            //         'price'             => 8.5, 
            //         'status_id'         => 2, 
            //         'activated_at'      => now(), 
            //         'expired_at'        => date('Y-m-d 23:59:59', strtotime('+30 days', strtotime(Date('Y-m-d')))), 
            //         'status_updater_id'      => 1
            //     ]);

            //     // ============================================== Add Store
            //     for($k = 1; $k <= 5; $k++){

            //         $userId = DB::table('user')->insertGetId([ 
            //             'type_id'           =>   3, 
            //             'uid'               =>  Account::generateUid(), 
            //             'email'             =>  'store-'.$i.$j.$k.'@mgtb.com',                   
            //             //'phone'             =>  '04000000'.$i.$j, 
            //             'password'          =>  bcrypt('123456'), 
            //             'is_active'         =>  1, 
            //             'is_email_verified' =>  1, 
            //             'is_phone_verified' =>  0,
            //             'name_kh' => 'Store '.$i.$j.$k,
            //             'name' => 'Store '.$i.$j.$k
            //         ]);

            //         $subscriptionId = rand(2, 4); 
            //         $memberAsStoreId = DB::table('member')->insertGetId([ 
            //             'user_id'       => $userId, 
            //             'sponsor_id'    => $memberAsDepotId,
            //             'province_id'   => $i, 
            //             'district_id'   => rand(1, 23), 
                     
            //             'subscription_id'   => $subscriptionId, 
                      
            //         ]);

            //         DB::table('member_subscriptions')->insertGetId([ 
            //             'member_id'         => $memberAsStoreId,
            //             'subscription_id'   => $subscriptionId, 
            //             'price'             => 10, 
            //             'status_id'         => 2, 
            //             'activated_at'      => now(), 
            //             'expired_at'        => date('Y-m-d 23:59:59', strtotime('+30 days', strtotime(Date('Y-m-d')))), 
            //             'status_updater_id'      => 1
            //         ]);
            //     }
            // }

        }
        

        
	}
}
