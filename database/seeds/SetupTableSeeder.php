<?php

use Illuminate\Database\Seeder;

class SetupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('subscription_status')->insert(
            [
                ['name' => 'Pending'],
                ['name' => 'Active'],
                ['name' => 'Expired'],
                ['name' => 'Rejected'], 
                ['name' => 'Terminated']
            ]
        );

        DB::table('role')->insert(
            [
                ['name' => 'Customer'],
                ['name' => 'Mobile'],
                ['name' => 'Depot'],
                ['name' => 'Branch'],
            ]
        );

       
        DB::table('subscription')->insert(
            [
                [
                   
                    'name'          => 'Member', 
                    'requirement'   => 'Purchase 1-19 Units', 
                    'unit_price'    => 10, 
                    'min_purchase'  => 0,
                    'max_purchase'  => 0,
                    'role'          => 'Mobile', 
                    //'role_id'       => 2, 
                    'icon'          => 'public/icon/1.png'
                   
                ],

                [
                    'name'          => 'Start up', 
                    'requirement'   => 'Purchase 20-59 Units', 
                    'unit_price'    => 12, 
                    'min_purchase'  => 20,
                    'max_purchase'  => 59,
                    'role'          => 'Mobile', 
                    //'role_id'       => 3, 
                    'icon'          => 'public/icon/2.png'
                   
                ],


                [
                    'name'          => 'Basic', 
                    'requirement'   => 'Purchase 60-99 Units', 
                    'unit_price'    => 11, 
                    'min_purchase'  => 60,
                    'max_purchase'  => 99,
                    'role'          => 'Mobile', 
                    //'role_id'       => 3, 
                    'icon'          => 'public/icon/3.png'
                   
                ],
               
                [
                    'name'          => 'Premium', 
                    'requirement'   => 'Purchase 100-249 Units',
                    'unit_price'    => 10, 
                    'min_purchase'  => 100,
                    'max_purchase'  => 249,
                    'role'          => 'Mobile', 
                    //'role_id'       => 3, 
                    'icon'          => 'public/icon/4.png'
                   
                ],

                [
                    'name'          => 'VIP', 
                    'requirement'   => 'Purchase 250-499 Units',
                    'unit_price'    => 8.5, 
                    'min_purchase'  => 250,
                    'max_purchase'  => 499,
                    'role'          => 'M/Depot', 
                    //'role_id'       => 4, 
                    'icon'          => 'public/icon/5.png'
                   
                ],

                [
                    'name'          => 'VVIP', 
                    'requirement'   => 'Purchase 500+ Units',
                    'unit_price'    => 8.5, 
                    'min_purchase'  => 500,
                    'max_purchase'  => 1000,
                    'role'          => 'M/Branch', 
                    //'role_id'       => 5, 
                    'icon'          => 'public/icon/6.png'
                   
                ],
               
            ]
        );

        DB::table('usd_transactions_type')->insert(
            [
                ['name' => 'Send'],
                ['name' => 'Receive']
            ]
        );

        DB::table('usd_transactions_category')->insert(
            [
                ['type_id' => 1,  'name' => 'Withdrawal',                   'action' => 'Sent'],
                ['type_id' => 2,  'name' => 'Roll Up Bonus',                'action' => 'Received'],
                ['type_id' => 2,  'name' => 'Depot Purchase',               'action' => 'Received'],
                ['type_id' => 2,  'name' => 'Store Purchase',               'action' => 'Received'],
                ['type_id' => 2,  'name' => 'Branch Cross Border Sale',     'action' => 'Received'],
                ['type_id' => 2,  'name' => 'Depot Cross Border Sale',      'action' => 'Received'],
                ['type_id' => 2,  'name' => 'Branch Cross Border Pay',     'action' => 'Sent'],
                ['type_id' => 2,  'name' => 'Depot Cross Border Pay',      'action' => 'Sent'],

            ]
        );

      

    }
}
