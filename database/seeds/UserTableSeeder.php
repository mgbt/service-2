<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       


        DB::table('users_type')->insert(
            [
                ['name' => 'admin'],
                ['name' => 'master'],
                ['name' => 'member'],
            ]
        );

        // ============================================================ MGTB Admin
        $userId = DB::table('user')->insertGetId([ 
            'type_id'=>1, 
            'email'=>'admin.mgtb@mgtb.com',                   
            'phone' => '010000001', 
            'password' => bcrypt('123456'), 
            'is_active'=>1, 
            'is_email_verified'=>1, 
            'is_phone_verified'=>1,
            'name' => 'MGTB'
        ]);

        $admin = DB::table('admin')->insert([[ 'user_id' =>$userId]]);

         // ============================================================ MGTB CamCyber
         $userId = DB::table('user')->insertGetId([ 
            'type_id'=>2, 
            'email'=>'admin.camcyber@mgtb.com',                   
            'phone' => '010000002', 
            'password' => bcrypt('123456'), 
            'is_active'=>1, 
            'is_email_verified'=>1, 
            'is_phone_verified'=>1,
            'name' => 'CamCyber'
        ]);

        $admin = DB::table('admin')->insert([[ 'user_id' =>$userId]]);
          
    }
}
