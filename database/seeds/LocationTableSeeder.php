<?php

use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
        DB::table('country')->insert(
            [
                //['abbre' => 'CN',  'name' => 'China ',           'flag' => '{"xs":"public/country/china.png"}'],
                ['abbre' => 'KH',  'name' => 'Cambodia ',        'flag' => '{"xs":"public/country/cambodia.png"}'],
                // ['abbre' => 'IN',  'name' => 'India ',           'flag' => '{"xs":"public/country/india.png"}'],
                // ['abbre' => 'LA',  'name' => 'Laos ',            'flag' => '{"xs":"public/country/laos.png"}'],
                // ['abbre' => 'MS',  'name' => 'Malaysia ',        'flag' => '{"xs":"public/country/malaysia.png"}'],
                // ['abbre' => 'PH',  'name' => 'Philippine ',      'flag' => '{"xs":"public/country/philipine.png"}'],
                // ['abbre' => 'SG',  'name' => 'Singapore ',       'flag' => '{"xs":"public/country/singapore.png"}'],
                // ['abbre' => 'SK',  'name' => 'South Korea ',     'flag' => '{"xs":"public/country/south-korea.png"}'],
                // ['abbre' => 'TH',  'name' => 'Thailand ',        'flag' => '{"xs":"public/country/thailand.png"}'],
                // ['abbre' => 'US',  'name' => 'United States ',   'flag' => '{"xs":"public/country/united-states.png"}'],
                // ['abbre' => 'VN',  'name' => 'Vietnam',          'flag' => '{"xs":"public/country/vietnam.png"}']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'បន្ទាយមានជ័យ']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកមង្គលបូរី'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកភ្នំស្រុក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកព្រះនេត្រព្រះ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកអូរជ្រៅ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងសិរីសោភ័ណ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកថ្មពួក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្វាយចេក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកម៉ាឡៃ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងប៉ោយប៉ែត']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'បាត់ដំបង']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកបាណន់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកថ្មគោល'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងបាត់ដំបង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកបវេល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកឯកភ្នំ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកមោងឫស្សី'],
                ['province_id' => $provinceId, 'name' => 'ស្រុករតនមណ្ឌល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសង្កែ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសំឡូត'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសំពៅលូន'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកភ្នំព្រឹក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំរៀង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកគាស់ក្រឡ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុករុក្ខគិរី'],
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'កំពង់ចាម']);
         DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកបាធាយ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកចំការលើ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកជើងព្រៃ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងកំពង់ចាម'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំពង់សៀម'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកងមាស'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកោះសូទិន'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកព្រៃឈរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្រីសន្ធរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្ទឹងត្រង់']
            ]
            );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'កំពង់ឆ្នាំង']);
         DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកបរិបូណ៌'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកជលគីរី'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងកំពង់ឆ្នាំង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំពង់លែង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំពង់ត្រឡាច'],
                ['province_id' => $provinceId, 'name' => 'ស្រុករលាប្អៀរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសាមគ្គីមានជ័យ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកទឹកផុស']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'កំពង់ស្ពឺ']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកបរសេដ្ឋ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងច្បារមន'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកគងពិសី'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកឱរ៉ាល់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកឧដុង្គ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកភ្នំស្រួច'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសំរោងទង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកថ្ពង']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'កំពង់ធំ']);
         DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកបារាយណ៍'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំពង់ស្វាយ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងស្ទឹងសែន'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកប្រាសាទបល្ល័ង្'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកប្រាសាទសំបូរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសណ្ដាន់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសន្ទុក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្ទោង']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'កំពត']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកអង្គរជ័យ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកបន្ទាយមាស'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកឈូក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកជុំគិរី'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកដងទង់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំពង់ត្រាច'],
                ['province_id' => $provinceId, 'name' => 'ស្រុក​ទឹក​ឈូ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុង​កំពត']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'កណ្តាល']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកកណ្ដាលស្ទឹង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកៀនស្វាយ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកខ្សាច់កណ្ដាល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកោះធំ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកលើកដែក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកល្វាឯម'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកមុខកំពូល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកអង្គស្នួល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកពញាឮ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្អាង'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងតាខ្មៅ']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'កោះកុង']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកបទុមសាគរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកគិរីសាគរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកោះកុង'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងខេមរភូមិន្'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកមណ្ឌលសីមា'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្រែអំបិល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកថ្មបាំង']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ក្រចេះ']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកឆ្លូង'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងក្រចេះ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកព្រែកប្រសព្'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសំបូរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្នួល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកចិត្របុរី']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'មណ្ឌលគិរី']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកកែវសីមា'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកោះញែក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកអូររាំង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកពេជ្រាដា'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងសែនមនោរម្']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ភ្នំពេញ']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌចំការមន'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌដូនពេញ'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌប្រាំពីរមករា'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌទួលគោក'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌដង្កោ'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌមានជ័យ'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌឫស្សីកែវ'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌសែនសុខ'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌពោធិ៍សែនជ័យ'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌជ្រោយចង្វារ'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌព្រែកព្នៅ'],
                ['province_id' => $provinceId, 'name' => 'ខណ្ឌច្បារអំពៅ']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ព្រះវិហារ']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកជ័យសែន'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកឆែប'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកជាំក្សាន្'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកគូលែន'],
                ['province_id' => $provinceId, 'name' => 'ស្រុករវៀង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសង្គមថ្មី​'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកត្បែងមានជ័យ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងព្រះវិហារ']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ព្រៃវែង']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកបាភ្នំ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំចាយមារ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំពង់ត្របែក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកញ្ជ្រៀច'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកមេសាង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកពាមជរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកពាមរក៍'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកពារាំង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកព្រះស្ដេច'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងព្រៃវែង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកពោធិ៍រៀង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស៊ីធរកណ្ដាល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្វាយអន្ទរ']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ពោធិ៍សាត់']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកបាកាន'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកណ្តៀង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកក្រគរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកភ្នំក្រវាញ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងពោធិ៍សាត់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកវាលវែង']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'រតនគិរី']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកអណ្ដូងមាស'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងបានលុង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកបរកែវ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកូនមុំ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកលំផាត់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកអូរជុំ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកអូរយ៉ាដាវ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកតាវែង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកវើនសៃ']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'សៀមរាប']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកអង្គរជុំ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកអង្គរធំ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកបន្ទាយស្រី'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកជីក្រែង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកក្រឡាញ់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកពួក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកប្រាសាទបាគង'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងសៀមរាប'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសូទ្រនិគម'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្រីស្នំ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្វាយលើ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកវ៉ារិន']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ព្រះសីហនុ']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ក្រុងព្រះសីហនុ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកព្រៃនប់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្ទឹងហាវ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំពង់សិលា']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ស្ទឹងត្រែង']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកសេសាន'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសៀមបូក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសៀមប៉ាង'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងស្ទឹងត្រែង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកថាឡាបរិវ៉ាត់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកបុរីអូរស្វាយសែនជ័យ']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ស្វាយរៀង']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកចន្ទ្'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកំពង់រោទិ៍'],
                ['province_id' => $provinceId, 'name' => 'ស្រុករំដួល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុករមាសហែក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្វាយជ្រំ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងស្វាយរៀង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកស្វាយទាប'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងបាវិត']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'តាកែវ']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកអង្គរបូរី'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកបាទី'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកបូរីជលសារ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកគីរីវង់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកកោះអណ្ដែត'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកព្រៃកប្បាស'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសំរោង'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងដូនកែវ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកត្រាំកក់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកទ្រាំង']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ឧត្តរមានជ័យ']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកអន្លង់វែង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកបន្ទាយអំពិល'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកចុងកាល់'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងសំរោង'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកត្រពាំងប្រាសាទ']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'កែប']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកដំណាក់ចង្អើរ'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងកែប']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ប៉ៃលិន']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ក្រុងប៉ៃលិន​'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកសាលាក្រៅ']
            ]
        );

        // ============================================================== 
        $provinceId = DB::table('province')
        ->insertGetId(['country_id' => 1, 'name' => 'ត្បូងឃ្មុំ']);
        DB::table('district')->insert(
            [
                ['province_id' => $provinceId, 'name' => 'ស្រុកតំបែរ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកក្រូចឆ្មារ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកមេមត់'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកអូររាំងឪ'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកពញាក្រែក'],
                ['province_id' => $provinceId, 'name' => 'ស្រុកត្បូងឃ្'],
                ['province_id' => $provinceId, 'name' => 'ក្រុងសួង']
            ]
        );
        
	}
}
