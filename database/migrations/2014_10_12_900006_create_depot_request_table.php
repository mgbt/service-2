<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepotRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depot_request', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('branch_id')->unsigned()->index()->default(1);
            $table->integer('depot_id')->unsigned()->index()->nullable();
            $table->integer('district_id')->unsigned()->index()->nullable();

            $table->datetime('accepted_at')->nullable();
            $table->datetime('rejected_at')->nullable();
            $table->integer('action_by')->nullable();//ID of admin from tabel User
            
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depot_request');
    }
}
