<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_subscriptions', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('member_id')->unsigned()->index()->nullable();
            $table->foreign('member_id')->references('id')->on('member')->onDelete('cascade');

            $table->integer('subscription_id')->unsigned()->index()->nullable();
            $table->foreign('subscription_id')->references('id')->on('subscription');
            
            $table->decimal('price', 10, 2)->default(0);
            
            $table->dateTime('activated_at')->nullable();
            $table->dateTime('expired_at')->nullable();

           
            $table->integer('status_updater_id')->unsigned()->index()->nullable();
            $table->integer('status_id')->unsigned()->index()->nullable();
            $table->foreign('status_id')->references('id')->on('subscription_status'); // 1: Pending; 2: Accepted; 3: Rejected; 4: Terminate
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_subscriptions');
    }
}
