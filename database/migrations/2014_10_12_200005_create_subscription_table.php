<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription', function (Blueprint $table) {
            $table->increments('id', 11);
            
           
            $table->string('name', 150)->default('');
            $table->string('requirement', 150)->default('');
            $table->decimal('unit_price', 10, 2)->nullable();
            $table->decimal('min_purchase', 10, 2)->nullable();
            $table->decimal('max_purchase', 10, 2)->nullable();
            $table->string('role', 150)->default('');
            $table->string('icon', 150)->default('');

            // $table->integer('role_id')->unsigned()->index()->nullable();
            // $table->foreign('role_id')->references('id')->on('role')->onDelete('cascade');
           
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription');
    }
}
