<?php

$controller = 'Subscription\Controller@';

$api->get('/my', 				        ['uses'     => $controller.'mySubscriptions']);
$api->get('/', 				            ['uses'     => $controller.'listing']);
$api->post('/', 				            ['uses'     => $controller.'subscrbe']);

