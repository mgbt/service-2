<?php

	
$api->group(['namespace' => 'App\Api\V1\Controllers\Member'], function($api) {
	
	
	$api->group(['prefix' => 'dashboard'], function ($api) {
		require(__DIR__.'/dashboard.php');
	});

	$api->group(['prefix' => 'orders'], function ($api) {
		require(__DIR__.'/order.php');
	});

	$api->group(['prefix' => 'packages'], function ($api) {
		require(__DIR__.'/package.php');
	});

	$api->group(['prefix' => 'products'], function ($api) {
		require(__DIR__.'/product.php');
	});

	$api->group(['prefix' => 'customers'], function ($api) {
		require(__DIR__.'/customer.php');
	});

	$api->group(['prefix' => 'purchases'], function ($api) {
		require(__DIR__.'/purchase.php');
	});

	$api->group(['prefix' => 'subscriptions'], function ($api) {
		require(__DIR__.'/subscribtion.php');
	});

	$api->group(['prefix' => 'transactions'], function ($api) {
		require(__DIR__.'/transaction.php');
	});
	$api->group(['prefix' => 'my-profiles'], function ($api) {
		require(__DIR__.'/my-profile.php');
	});
	$api->group(['prefix' => 'security'], function ($api) {
		require(__DIR__.'/my-profile.php');
	});


});
