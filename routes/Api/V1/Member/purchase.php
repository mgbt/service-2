<?php

$controller = 'Purchase\PurchaseController@';

$api->get('/', 				            ['uses' => $controller.'listing']);
$api->get('/{id}', 			            ['uses' => $controller.'view']);
$api->post('/', 				        ['uses' => $controller.'create']);
