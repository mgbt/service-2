<?php

// $api->get('/provinces', 			    ['uses' => 'Order\MemberController@provinces']);
// $api->get('/stores', 			        ['uses' => 'Order\MemberController@stores']);

$controller = 'Order\Controller@';
$api->get('/', 							['uses' => $controller.'listing']);
$api->get('/{id}', 						['uses' => $controller.'view']);
$api->post('/', 						['uses' => $controller.'create']);
