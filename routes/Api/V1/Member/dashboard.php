<?php

$controller = 'Dashboard\DashboardController@';
$api->get('/info', 				    ['uses' => $controller.'info']);
$api->get('/depots', 			    ['uses' => $controller.'getDepots']);
$api->get('/stores', 			    ['uses' => $controller.'getStores']);
$api->get('/get-account-info', 	    ['uses' => $controller.'getAccount']);
$api->get('/make-request', 	        ['uses' => $controller.'addDepot']);
$api->get('/sales', 	            ['uses' => $controller.'getPurchase']);
$api->get('/orders', 	            ['uses' => $controller.'getOder']);

