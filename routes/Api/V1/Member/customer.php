<?php

$controller = 'Customer\Controller@';

$api->get('/', 						            	['uses' => $controller.'listing']);
$api->post('/', 						            ['uses' => $controller.'create']);
$api->get('/provinces', 		                    ['uses' => $controller.'provinces']);
$api->get('/info', 				                    ['uses'    => $controller.'getAccount']);

$api->get('/root-node', 				            ['uses' => 'Customer\ChartController@rootNode']);
$api->get('/nodes', 				                ['uses' => 'Customer\ChartController@getNodes']);

