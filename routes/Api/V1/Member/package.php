<?php

$controller = 'Package\PackageController@';

$api->get('/', 							['uses'     => $controller.'listing']);
$api->get('/subscription-range', 		['uses'     => $controller.'subscriptionPrice']);
$api->get('/{id}', 			            ['uses'     => $controller.'view']);

$api->post('/', 			            ['uses'     => $controller.'create']);
$api->get('/{id}/stocks', 	            ['uses'     => $controller.'stocks']);


