<?php

$controller = 'Purchase\PurchaseController@';

$api->get('/', 				    ['uses' => $controller.'listing']);
$api->get('/check', 	['uses' => $controller.'checkPurchase']);
$api->post('/', 				['uses' => $controller.'action']);
