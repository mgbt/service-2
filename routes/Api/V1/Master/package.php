<?php

$controller = 'Package\PackageController@';

$api->get('/', 				['uses' => $controller.'listing']);
$api->get('/{id}', 			['uses' => $controller.'view']);
$api->put('/{id}', 			['uses' => $controller.'update']); //Update
$api->post('/', 			['uses' => $controller.'create']); //Add new
$api->delete('/{id}', 		['uses' => $controller.'delete']);
$api->get('/{id}/stocks', 	['uses'     => $controller.'stocks']);

