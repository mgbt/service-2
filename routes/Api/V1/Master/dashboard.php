<?php

$controller = 'Dashboard\DashboardController@';

$api->get('/info', 				['uses' => $controller.'info']);
$api->get('/get-buys', 			['uses' => $controller.'getBuy']);
$api->get('/get-sales', 		['uses' => $controller.'getSale']);