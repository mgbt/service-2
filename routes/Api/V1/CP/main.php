<?php

	
	$api->group(['namespace' => 'App\Api\V1\Controllers\CP'], function($api) {
		$api->group(['prefix' => 'quick-get'], function ($api) {
			require(__DIR__.'/quick-get.php');
		});
		$api->group(['prefix' => 'dashboard'], function ($api) {
			require(__DIR__.'/dashboard.php');
		});

		$api->group(['prefix' => 'admins'], function ($api) {
			require(__DIR__.'/admin.php');
		});
		$api->group(['prefix' => 'packages'], function ($api) {
			require(__DIR__.'/package.php');
		});
		$api->group(['prefix' => 'products'], function ($api) {
			require(__DIR__.'/product.php');
		});
		$api->group(['prefix' => 'members'], function ($api) {
			require(__DIR__.'/member.php');
		});
		
		$api->group(['prefix' => 'operation'], function ($api) {
			require(__DIR__.'/operation.php');
		});
		$api->group(['prefix' => 'setup'], function ($api) {
			require(__DIR__.'/setup.php');
		});

		 $api->group(['prefix' => 'orders'], function ($api) {
		 	require(__DIR__.'/order.php');
		 });
		 $api->group(['prefix' => 'purchases'], function ($api) {
			require(__DIR__.'/purchase.php');
		});

		$api->group(['prefix' => 'subscriptions'], function ($api) {
			// require(__DIR__.'/subscription.php');
		});
		
		$api->group(['prefix' => 'transactions'], function ($api) {
			require(__DIR__.'/transaction.php');
		});
	});