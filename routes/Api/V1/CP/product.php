<?php

$controller = 'Product\Controller@';

$api->get('/', 				            ['uses'     => $controller.'listing']);
$api->get('/{id}', 			            ['uses'     => $controller.'view']);
$api->put('/{id}', 			            ['uses'     => $controller.'update']); 
$api->post('/', 			            ['uses'     => $controller.'create']);
$api->delete('/{id}', 		            ['uses'     => $controller.'delete']);


// ===================== create images

$ProductImageController = 'Product\ProductImageController@';
$api->get('/{id}/images', 				            ['uses' => $ProductImageController.'listing']);
$api->post('/{id}/images', 			                ['uses' => $ProductImageController.'create']); 
$api->delete('/{id}/images/{product_images_id}',    ['uses' => $ProductImageController.'delete']); 

/////////// Package Prices

$PriceController = 'Product\PriceController@';
$api->get('/{id}/prices', 				            ['uses' => $PriceController.'listing']);
$api->post('/{id}/prices', 			                ['uses' => $PriceController.'create']); 
$api->put('/{packageId}/prices', 				    ['uses' => $PriceController.'updatePrice']);// Update 
$api->delete('/{id}/prices/{package_prices_id}',    ['uses' => $PriceController.'delete']); 

//////////Stock Packages

$StockController = 'Package\StockController@';
$api->get('/{id}/stocks', 				            ['uses' => $StockController.'listing']);