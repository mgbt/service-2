<?php

$controller = 'MyProfilex\Controller@';
$api->get('/', 						['uses' => $controller.'get']);
$api->put('/', 						['uses' => $controller.'put']);
$api->put('/change-password', 		['uses' => $controller.'changePassword']); //Update
$api->get('/logs', 					['uses' => $controller.'logs']); 