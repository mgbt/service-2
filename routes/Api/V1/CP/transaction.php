<?php

$controller = 'Transaction\USDController@';
$api->get('/categories', 			['uses'     => $controller.'categories']);
$api->get('/clearances/pay', 		['uses'     => $controller.'pay']);
$api->get('/clearances', 			['uses'     => $controller.'clearances']);
$api->get('/', 				        ['uses'     => $controller.'transactions']);

