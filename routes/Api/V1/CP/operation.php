<?php

$controller = 'Operation\Controller@';

$controller = 'Operation\Branch\PurchaseController@';
$api->get('/branch/purchases',			['uses' => $controller.'listing']);
$api->get('/branch/purchases/{id}',		['uses' => $controller.'view']);


$controller = 'Operation\Branch\OrderController@';
$api->get('/branch/orders',			['uses' => $controller.'listing']);
$api->get('/branch/orders/{id}',		['uses' => $controller.'view']);


$controller = 'Operation\Store\PurchaseController@';
$api->get('/store/purchases',			['uses' => $controller.'listing']);
$api->get('/store/purchases/{id}',		['uses' => $controller.'view']);


$controller = 'Operation\Store\OrderController@';
$api->get('/store/orders',			['uses' => $controller.'listing']);
$api->get('/store/orders/{id}',		['uses' => $controller.'view']);


$controller = 'Operation\Customer\OrderController@';
$api->get('/customer/orders',			['uses' => $controller.'listing']);
$api->get('/customer/orders/{id}',		['uses' => $controller.'view']);



$controller = 'Operation\Member\MemberController@';
$api->get('/member/purchases',			['uses' => $controller.'listing']);
$api->get('/member/purchases/{id}',		['uses' => $controller.'view']);

