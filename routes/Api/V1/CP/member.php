<?php

$api->post('/requests/{id}', 				        ['uses'     => 'Member\RequestController@action']);
$api->get('/requests', 				                ['uses'     => 'Member\RequestController@listing']);
$api->get('/convert-to-member/{id}', 				['uses'     => 'Member\RequestController@ConvertToMember']);


$controller = 'Member\MemberController@';
$api->get('/fillter', 				    ['uses'     => $controller.'fillter']);
$api->get('/', 				            ['uses'     => $controller.'listing']);
$api->get('/provinces', 		        ['uses'     => $controller.'provinces']);
$api->get('/{province_id}/districts', 	['uses'     => $controller.'districts']);  
$api->get('/{id}', 			            ['uses'     => $controller.'view']);
$api->put('/{id}', 			            ['uses'     => $controller.'update']); 
$api->post('/', 			            ['uses'     => $controller.'create']);
$api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
$api->put('/{id}/update-password', 		['uses'     => $controller.'updatePassword']);

$controller = 'Member\AccountInformationController@';
$api->get('/{Id}/accounts', 				       ['uses'     => $controller.'info']);


$api->group(['prefix' => 'accounts'], function ($api) {

    $controller = 'Member\AccountController@';
    $api->get('/', 				                        ['uses'     => $controller.'listing']); 
    $api->post('/', 			                        ['uses'     => $controller.'create']);
    $api->get('/package-info', 				            ['uses'     => $controller.'packageInfo']);
    $api->get('/info', 				                    ['uses'     => $controller.'getAccount']);

});

//////////////////////////// Package

$PackageController = 'Member\PackageController@';
$api->get('/{memberId}/avaiable-packages', 				        ['uses' => $PackageController.'availablePackages']); // Products that has not been in this pakcage
$api->get('/{memberId}/packages', 						        ['uses' => $PackageController.'listing']); // Added Products for this package
$api->get('/{memberId}/packages/{packageId}', 	                ['uses' => $PackageController.'action']); // Add or Remove product
