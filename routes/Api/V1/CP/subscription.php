<?php

$controller = 'Subscription\SubscriptionController@';

$api->get('/', 				['uses' => $controller.'listing']);
$api->get('/{id}', 			['uses' => $controller.'view']);
$api->put('/{id}', 			['uses' => $controller.'update']);

