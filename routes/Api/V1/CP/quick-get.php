<?php

use Illuminate\Support\Facades\DB;

$api->get('/{table}', 			['as' => 'get', 			function($table){
	$data = DB::table($table)->select('*')->get();
	return response()->json($data, 200);
}]);

