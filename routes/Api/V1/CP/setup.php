<?php

$api->group(['prefix' => 'countries'], function ($api) {

    $controller = 'Setup\CountryController@';

    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
    
});

$api->group(['prefix' => 'provinces'], function ($api) {

    $controller = 'Setup\ProvinceController@';

    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
    
});

$api->group(['prefix' => 'districts'], function ($api) {

    $controller = 'Setup\DistrictController@';

    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
    $api->get('/provinces', 		        ['uses'     => $controller.'provinces']);
    
});

$api->group(['prefix' => 'roles'], function ($api) {

    $controller = 'Setup\RoleController@';

    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
    
});

$api->group(['prefix' => 'subscriptions'], function ($api) {

    $controller = 'Setup\SubscriptionController@';

    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
    
});

$api->group(['prefix' => 'provinces'], function ($api) {

    $controller = 'Setup\ProvinceController@';

    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
    
});

$api->group(['prefix' => 'package-category'], function ($api) {

    $controller = 'Setup\PackageCategoryController@';

    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
    
});

$api->group(['prefix' => 'product-category'], function ($api) {

    $controller = 'Setup\ProductCategoryController@';

    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
    
});

