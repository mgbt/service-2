<?php

namespace App\Api\V1\Controllers\Member\Package;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Api\V1\Controllers\ApiController;

use App\Model\Package\Main as Package;
use App\Model\Subscription\Main as Subscription;
use App\Model\Order\Detail as CustomerOrder;
use App\Model\Purchase\Detail as MemberPurchase;


// Import Resource
use App\Api\V1\Resources\Package\PackageResource;
use App\Api\V1\Resources\Package\PackagesCollection;

class PackageController extends ApiController
{
    use Helpers;
   
    function listing(Request $req){
       
        $user       = JWTAuth::parseToken()->authenticate();
      
        
         $data           = Package::select('id', 'sku','kh_name', 'en_name','image','discount','is_single_product', 'n_of_units', 'selling_price as price', 'updated_at')
        ->with([
           
            'prices',
            'products',
            'memberStock' => function($query) use ($user){
                $query->where('member_id', $user->member->id); 
            },
           // 'info'
        
           
        ])
        ->whereHas('products')
      
        ->withCount([
            'products as n_of_products',
        ])
        ; 
        

        // Condition for Branch/Depot
        if($user->member->branch_id || $user->member->depot_id){

           // return $user->member->id; 
            $data = $data->whereHas('branches', function($query) use ($user){
                $branchId = 0; 
                if($user->member->branch_id){
                    $branchId = $user->member->id; 
                }elseif($user->member->depot_id){
                    $branchId = $user->member->branch_owner_id; 
                }

                $query->where('branch_id', $branchId);   
            }) ;
        }

      

        // Search by Key
        if($req->key && $req->key != "") {
            $data = $data->where(function ($query) use ($req) {
            $query->where('kh_name', 'like', '%' . $req->key . '%')
            ->orWhere('en_name', 'like', '%' . $req->key . '%')
            ;
            });
        }
        
         // search type
        if($req->type){
            if($req->type == 'package'){
                $data = $data->where('is_single_product', 0); 
            }else if($req->type == 'product'){
                $data = $data->where('is_single_product', 1); 
            }
        }

    
        $data = $data->orderBy('is_single_product', 'asc')->paginate( $req->limit ? $req->limit : 10);
        $data = new PackagesCollection($data);
        return $data;
    }

    function stocks(Request $request, $packageId = 0){

        $user       = JWTAuth::parseToken()->authenticate();
        $limit          = intval( $request->limit ? $request->limit : 10); 
        if($request->type == 'order'){

            $order = CustomerOrder::select('id', 'order_id as trx_number', 'order_id', 'unit_price', 'qty', 'updated_at')
            ->with([
                'order:id,member_id,buyer_id', 
                'order.member:id,user_id', 
                'order.member.user:id,name,phone,email'
            ])
            ->where('package_id', $packageId); 

            if($request->key){
                $order->where('order_id', $request->key); 
            } 

            $order= $order->orderBy('id', 'desc')->paginate($limit);
            return response()->json($order, 200);

        }else if($request->type == 'purchase'){

            $purchase = MemberPurchase::select('id', 'purchase_id as trx_number', 'purchase_id', 'unit_price', 'qty', 'updated_at')
            ->with([
                'purchase:id,member_id', 
                'purchase.member.user:id,name,phone,email'
            ])
            ->where('package_id', $packageId);  

            if($request->key){
                $order->where('purchase_id', $request->key); 
            }
            $purchase= $purchase->orderBy('id', 'desc')->paginate($limit);
            return response()->json($purchase, 200);

        }   
       
    }

    function view($id = 0){

        $data   = Package::select('*')
        ->with([
            'category',
            'products', 
            'prices'
           
        ])
        ->where('is_single_product', 0)
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
        
    }

    function subscriptionPrice(Request $req){
        return Subscription::select('name', 'min_purchase as min', 'max_purchase as max')->where('max_purchase', '>', 0)->get(); 
    }

   
    
}
