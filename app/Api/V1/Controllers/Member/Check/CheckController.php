<?php

namespace App\Api\V1\Controllers\Member\Check;



use App\Api\V1\Controllers\ApiController;
use App\Model\Member\Subscription;
use App\Model\Purchase\Main as Purchase;
use Dingo\Api\Routing\Helpers;

class CheckController extends ApiController
{
    use Helpers;
    function listing() {
      
        $data = Subscription::select('*')
        ->where('expired_at', '<=', date('Y-m-d H:i:s'))
        ->where('status_id', 2) //Active
        ->where('subscription_id', '>', 1)
        ->get();

        //return $data; 

        $expiries = []; 

        foreach($data as $row){
            $expiry['expired_subscription'] = $row; 

            $row->status_id = 3; //Expired; 
            $row->save(); 

            //Check if this member has latest active purchase.
            $subscriptionId = 1; //Member
            $purchase = Purchase::where([
                'member_id' => $row->member_id
            ])
            ->where('subscription_expired_at', '>', date('Y-m-d H:i:s'))
            ->whereNull('activation_id')
            ->where('approved_at', '<>', null)
            ->orderBy('subscription_id', 'DESC')
            ->first()
            ; 

          
            $expired = date('Y-m-d 23:59:59', strtotime('+30 days', strtotime(Date('Y-m-d')))); 
            if($purchase){
                $subscriptionId = $purchase->subscription_id; 
                $expired = $purchase->subscription_expired_at; 

                $expiry['backup_pruchase'] = $purchase; 

            }

            $row->member->subscription_id = $subscriptionId; 
            $row->member->save(); 


            // Create Histrory; 

            $subscription                       = new Subscription; 
            $subscription->member_id            = $row->member_id; 
            $subscription->status_id            = 2; // Active 
            $subscription->subscription_id      = $subscriptionId; 
            $subscription->activated_at         = now(); 
            $subscription->expired_at           = $expired; 
            $subscription->save();

            if($row->member->contract_expired){
                $isStructureActive = 0; 
                $structureActivatedAt = null; 
                

                if($purchase){
                    if($row->member->branch && $purchase->subscription_id == 6 ){
                        $isStructureActive = 1; 
                        $structureActivatedAt = now(); 
                        $expiry['branch_can_continue'] = 1; 
                    }elseif($row->member->depot && $purchase->subscription_id >= 5 ){
                        $isStructureActive = 1; 
                        $structureActivatedAt = now(); 
                        $expiry['depot_can_continue'] = 1; 
                    }

                    $purchase->activation_id = $subscription->id; 
                    $purchase->id; 
                    
                }

                $row->member->is_structure_active       = $isStructureActive; 
                $row->member->structure_activated_at    = $structureActivatedAt; 
                $row->member->save(); 

                $expiry['member'] = $row->member; 

              
            }

            $expiries[$row->member->id] = $expiry; 
        }

        return response()->json($expiries, 200);
    }


}
