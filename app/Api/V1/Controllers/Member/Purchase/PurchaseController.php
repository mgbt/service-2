<?php

namespace App\Api\V1\Controllers\Member\Purchase;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use App\Model\Purchase\Main as Purchase;
use App\Model\Package\Main as Package;
use App\Model\Subscription\Main as SubscriptionPackage;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\MGTB\Bot\BotPurchase;

class PurchaseController extends ApiController
{

    use Helpers;
    function listing(Request $req) {
        $user       = JWTAuth::parseToken()->authenticate();

        $data = Purchase::select('*')
        ->with([
            'details',
            'subscription', 
            'activation',
        ])
        ->where('member_id', $user->member->id);

        // API search Date
        $from=isset($_GET['from'])?$_GET['from']:""; 
        $to=isset($_GET['to'])?$_GET['to']:"";
        if(isValidDate($from)){
            if(isValidDate($to)){
                
                $appends['from'] = $from;
                $appends['to'] = $to;

                $from .=" 00:00:00";
                $to .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }
        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }

    function view($receiptNumber=''){
        $user       = JWTAuth::parseToken()->authenticate();
        $data = Purchase::Select('*')
        ->with([
            'member',
            'member.province',
            'member.district',
            'member.refferal',
            'member.sponsor',
            'member.depot',
            'member.branch',
            'member.subscription',
            'details',
            'subscription'
        ])
        ->where('receipt_number', $receiptNumber)
        ->first();
        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => "Invalid Data", 
            ], 400);
        }
        return response()->json($data, 200);
    }

    function create(Request $request){

        //Check if this account is member
        $this->validate($request, [
            'cart' => 'required'
        ]);

        $user         = JWTAuth::parseToken()->authenticate();
        $cart         = json_decode($request->cart);
      
        if(sizeof($cart)>0){

           
            // Customer Cannot Make Purchase.
            if(!$user->member->subscription_id){
                return response()->json([
                    'status' => 'error',
                    'message' => "លោកអ្នកមិនអាចបញ្ជាទិញបានទេ។ សូមទំនាក់ទំនង MGTB ដើម្បីក្លាយជាសមាជិក", 
                ], 400);
            }

            $details            = [];
            $totalUnits         = 0;
            $totalPrice         = 0;

            $purchase                   = New Purchase; 
            $purchase->member_id        = $user->member->id;
            $purchase->purchased_at     = now(); 
            $purchase->save();
            
            //Loop to Get total Unit first
            foreach($cart as $packageId => $qty){
                $package = Package::whereHas('products')
                ->find($packageId)
                ; 
                if($package) {
                    $totalUnits += $qty*$package->n_of_units; 
                }
            }

            foreach($cart as $packageId => $qty){

                $package = Package::whereHas('products')
                ->find($packageId)
                ; 

                if($package){

                    $unitPrice = $this->getPriceByQty($package, $totalUnits); 
                   
                   
                    $detail = [
                        'purchase_id'   => $purchase->id,
                        'package_id'    => $package->id,
                      
                        'unit_price'    => $unitPrice, 
                        'qty'           => $qty, 
                        'n_of_units'    => $package->n_of_units, 
                        'discount'      => 0, 
                        'note'          => ''
                    ]; 

                    $totalPrice += $qty*$unitPrice; 
                    $details[] = $detail; 
                }   
              
            }


            if( $totalUnits > 0){
                    //Find Subscription
                    $subscription = SubscriptionPackage::where('min_purchase', '<=', $totalUnits)->where('max_purchase', '>=', $totalUnits)->first();
            
                    if($subscription){
                        $purchase->subscription_id = $subscription->id; 
                    }
    
                    $purchase->details()->insert($details);

                    $purchase->total_price = $totalPrice; 
                    $purchase->total_units = $totalUnits; 
                    $purchase->receipt_number = $this->getReceiptNumber(); 
                    $purchase->subscription_expired_at = date('Y-m-d 23:59:59', strtotime('+30 days', strtotime(Date('Y-m-d')))); 
    
                    $purchase->save(); 

                     $botRes = BotPurchase::new($purchase); 
    
                    return response()->json([
                        'status' => 'success',
                        'message' => 'ការទិញត្រូវបានដាក់។',
                        'purchase' => $purchase, 
                        'details' => $details, 
                        'bot' =>  $botRes
                    ], 200);
            }else{

                $purchase->delete(); 
                return response()->json([
                    'status' => 'error',
                    'message' => "Invalid purchaser", 
                    'totalUnits' => $totalUnits, 
                    'cart' => $cart
                ], 400);
            }

            

        }else{
            return response()->json([
                'status' => 'error',
                'message' => "Invalid Cart", 
            ], 422);
        }
       
    }

    function getPriceByQty($package, $nOfUnits = 0){

        $price = 0; 

        if($package){
            $prices = $package->prices; 
            foreach($prices as $row){
                if($nOfUnits >= $row->from && $nOfUnits <= $row->to){
                    $price = $row->price; 
                }
            }
        }
        

        return $price; 
    }

    function getReceiptNumber(){
        $number = rand(1000000000, 99999999); 

        $check = Purchase::where('receipt_number', $number)->first(); 
        if($check){
            return $this->getReceiptNumber(); 
        }

        return $number; 
    }

}
