<?php

namespace App\Api\V1\Controllers\Member\Transaction;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Carbon\Carbon;

use App\Api\V1\Controllers\ApiController;
use App\Model\Transaction\USD;
use App\Model\Transaction\USDCategory;


//========================== Use Mail
use App\MGTB\Wallet; 

class USDController extends ApiController
{
    use Helpers;

    function categories(){
        $data = USDCategory::select('id', 'name')->get(); 
        return $data; 
    }

    function transactions(Request $req){

        $data           = USD::select('id', 'category_id', 'type_id', 'member_id', 'amount', 'balance', 'description', 'created_at')
        ->with([
            'category:id,name'
        ]);
        
        $user       = JWTAuth::parseToken()->authenticate();
        $data       = $data->where('member_id', $user->member->id); 
        
       
        
        // ==============================>> Date Range
        if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        $key       =   isset($_GET['key'])?$_GET['key']:"";

        if( $key != "" ){
            $data = $data->where(function($query) use ($key){
                $query->where('id', 'like', '%'.$key.'%');
            });   
        }

        $category      =   intval(isset($_GET['category'])?$_GET['category']:0);
        if($category != 0){
            $data= $data->where('category_id', $category);
        }

        $type      =   intval(isset($_GET['type'])?$_GET['type']:0);
        if($type != 0){
            $data= $data->where('type_id', $type);
        }

        $limit          = intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json($data, 200);
    }

    function balance(){

        $user       = JWTAuth::parseToken()->authenticate();
        $balance = Wallet::checkBalance($user->uid, 'usd'); 
        return $balance; 
    }

    function summary(){
        $user       = JWTAuth::parseToken()->authenticate();
      
        return [
            'balance' => $this->balance(), 
            'date_range' => $this->dateRange($user), 
            'life_time' => $this->lifeTime($user)
        ]; 
        
    
    }

    function dateRange($user){

        $start =  Carbon::parse('today')->subMonths(1)->format('Y-m-d 00:00:00');
        $end = Carbon::parse('today')->format('Y-m-d 23:59:59:00'); 
        //return $end; 

        $categories = USDCategory::select('id', 'name')
        ->withCount([
            'transactions as total_amount' => function($query) use ($user, $start, $end){
                $query->select(DB::raw('sum(amount)'))
                ->where('member_id', $user->member->id)
                ->whereBetween('created_at', [$start, $end])
                ;
            }
        ])
        ->get(); 
        return $categories; 
    }

    function lifeTime($user){
        $categories = USDCategory::select('id', 'name')
        ->withCount([
            'transactions as total_amount' => function($query) use ($user){
                $query->select(DB::raw('sum(amount)'))
                ->where('member_id', $user->member->id)
                ;
            }
        ])
        ->get(); 
        return $categories; 
    }


   

}
