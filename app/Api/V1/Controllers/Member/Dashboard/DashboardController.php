<?php

namespace App\Api\V1\Controllers\Member\Dashboard;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Carbon\Carbon;
use App\MGTB\Bot\BotDepot;

use App\Api\V1\Controllers\ApiController;
use App\Model\Member\Main as Member;
use App\Model\Member\DepotRequest;
use App\Model\Member\Subscription as MemberSubscription;
use App\Model\Purchase\Main as Purchase;
use App\Model\Order\Main as Order;
use App\Model\Setup\District;

use App\MGTB\Wallet; 

class DashboardController extends ApiController
{
    use Helpers;
    function info(){
        
        $user = JWTAuth::parseToken()->authenticate(); 
        
        //return $user->member->subscription->role; 

        $location = '';
        if($user->member->province && $user->member->district){
            $location = $user->member->province->name .'-'.$user->member->district->name; 
        }

        $info = [
        	'uid'          => $user->uid, 
            'name'         => $user->name,
            'names'         => $user->name_kh,  
            'phone'        => $user->phone, 
            'sponsor_link' => 'http://demo.allbioshop.com/member/#/auth/register?ref='.$user->uid, 
            'email'        => $user->email, 
            'location'     => $location,  
        ]; 
        $member   =Member::select('*')
        ->with([
            'refferal',
            'sponsor'
        ])
        ->where([
            'user_id' => $user->id, 
        ])
        ->get();
        $wallet['usd'] = Wallet::checkBalance($user->member->id); 

        $subscription           = MemberSubscription::select('*')
        ->addSelect(DB::raw("DATEDIFF(expired_at, NOW()) AS expired_countdown"))
        ->where([
            'member_id' => $user->member->id, 
        ])
        ->whereIn('status_id', [2])
        ->with([
            'subscription',

        ])
        ->orderBy('id', 'desc')
        ->first();

        $structure = ""; 
        if($subscription){
            if(isset($user->member->branch)){
                $structure = $user->member->branch->name; 

            }elseif(isset($user->member->depot)){
                $structure = $user->member->depot->province->name .'-'. $user->member->depot->name; 
            }

            if($structure != ""){
                if(in_array($subscription->subscription_id, [1, 2, 3, 4])){
                    $structure .= '(Inactive)';
                }
            }
           
        }

        return response()->json([
            'info'      => $info,
            'member'    => $member,
            'wallet'    => $wallet, 
            'subscription'   => $subscription, 
            'purchase'  => $user->member->purchases()->where(['counted_at' => null])->sum('total_units'), 
            'structure' => $structure, 
            'depot_owner' => $user->member->depotOwner
        ], 200);
    }

    function getDepots(){

        $user = JWTAuth::parseToken()->authenticate(); 
        $depots = []; 
        if(isset($user->member->branch)){
           
            // Check for depot
            $data = District::select('id', 'name')
            ->where([
                'province_id' => $user->member->branch->id
            ])
            ->with([
                'depots' => function($query) use ($user){
                    $query->where('branch_owner_id', $user->member->id); 
                }
            ])
            ->get(); 


            foreach($data as $row){
                $depot['district'] = $row->name; 
                $depot['id'] = $row->id; 
                
                if(isset($row->depots[0])){
                    $depot['depot'] = $row->depots[0]; 
                }else{
                    // Check having requesting
                    $depotRequest = DepotRequest::where([
                        'branch_id' => $user->member->id, 
                        'district_id' => $row->id
                    ])
                    ->where('action_by', null)
                    ->first();

                    if($depotRequest){
                        $depot['request'] = $depotRequest->depot; 
                    }
                }

                $depots[] = $depot; 
                unset($depot); 
            }

        }

        return $depots; 
    }

    function getStores(){
        $user = JWTAuth::parseToken()->authenticate(); 
        $stores = Member::select('id', 'user_id', 'sponsor_id', 'subscription_id')
        ->with([
            'user', 
            'subscription'
        ])
        ->where([
            'sponsor_id' => $user->member->id
        ])
        ->whereIn('subscription_id', [2,3,4])
        ->get(); 

        return $stores; 
    }

  
    function getAccount(Request $req){
        $user = JWTAuth::parseToken()->authenticate(); 

        $member = Member::select('id', 'user_id', 'branch_id', 'depot_id', 'subscription_id')
        ->whereHas('user', function($query) use ($req){
            $query->where('uid', $req->uid); 
        })
        ->with([
            'user:id,name,uid,email,phone', 
            'subscription'
        ])
        ->whereDoesntHave('depotRequests', function($query) use ($user){
            $query->whereHas('district', function($query) use ($user){
                $query->where('province_id', $user->member->branch_id); 
            }); 
        })
        ->first();


        if($member){

            return response()->json([
                'status' => 'success', 
                'data'=> $member
            ], 200);

        }else{
            return response()->json([
                'status' => 'fail', 
                'message' => 'លេខសម្គាល់អ្នកប្រើមិនត្រឹមត្រូវ'
            ], 200);
        }

   
    }

    function addDepot(Request $req){
        $user = JWTAuth::parseToken()->authenticate(); 
        // Check if this account is logged account is branch
        if(isset($user->member->branch)){

            // Check if submited district is under this branch's province; 
            $district = District::where([
                'province_id' => $user->member->branch->id, 
                'id' => $req->district_id
            ])->first(); 
            if($district){
                // Check if UID is valid Member account
                $member = Member::select('id', 'user_id', 'branch_id', 'depot_id', 'subscription_id')
                ->whereHas('user', function($query) use ($req){
                    $query->where('uid', $req->uid); 
                })
                ->with([
                    'user:id,name,uid,email,phone', 
                    'subscription'
                ])
                ->first();
                if($member){
                    // Check if this account does not have pending auction
                    if(!isset($member->depot_id) && !isset($member->branch_id)){

                        //Check if having exiting request within this district. Then cancel.
                        $depotRequest = DepotRequest::where([
                            'branch_id'     => $user->member->id, 
                            'depot_id'      => $member->id, 
                            'district_id'   => $req->district_id
                        ])
                        ->first();
                        if($depotRequest){

                            if(!isset($depotRequest->action_by)){
                                //Delete
                                $depotRequest->delete(); 
                                return response()->json([
                                    'message'      => 'សំណើរបស់អ្នកត្រូវបានបោះបង់។', 
                                ], 200);
                            }else{
                                   
                                return response()->json([
                                    'message'      => 'Callelation cannot be made', 
                                ], 200);
                            }
                        }else{
                            //Adding record to database
                            $depotRequest = new DepotRequest; 
                            $depotRequest->branch_id    = $user->member->id;  //MemberID
                            $depotRequest->depot_id     = $member->id; 
                            $depotRequest->district_id  = $req->district_id; 
                            $depotRequest->created_at  = now(); 
                            $depotRequest->save(); 

    
                            $botRes = BotDepot::new($depotRequest);

                            return response()->json([
                                'message'      => 'សំណើរបស់អ្នកត្រូវបានដាក់ស្នើ', 
                                'data' => $depotRequest,
                                'bot' =>  $botRes
                            ], 200);
                        }
                    }else{
                        return response()->json(['message'=> 'សមាជិកនេះមិនអាចទទួលយកជាឃ្លាំងបានទេ។'], 400);
                    }
                }else{
                    return response()->json(['message'=> 'សមាជិកមិនត្រឹមត្រូវ'], 400);
                }
            }else{
                return response()->json(['message'=> 'ស្រុកមិនត្រឹមត្រូវ'], 400);
            }

            
        }else{
            return response()->json(['message'=> 'គណនីសាខាមិនត្រឹមត្រូវ'], 400);
        }
 
    }

    function getPurchase(Request $req,$memberId=0){
        $user = JWTAuth::parseToken()->authenticate();
         //return $user->member;
        $sales           = Purchase::select('*')
        ->with([
            'member'
        ])
        ->where('member_id', $user->member->id)
        ->limit(10)
        ->get();

        return $sales; 
    }
    
    function getOder(Request $req){
        $user = JWTAuth::parseToken()->authenticate(); 
        $order           = Order::select('*')
        ->with([
            'member'
        ])
        ->where('member_id', $user->member->id)
        ->limit(10)
        ->get();

        return $order; 
    }
   
}
