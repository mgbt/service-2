<?php

namespace App\Api\V1\Controllers\Member\Customer;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;


use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Carbon\Carbon;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\MGTB\Account;
use App\Model\User\Main as User;
use App\Model\Member\Main as Customer;
use App\Model\Member\Subscription as MemberSubscription;

use App\Model\Setup\Province;

class Controller extends ApiController
{
    use Helpers;
   
    function listing(Request $request){
        
        $user     = JWTAuth::parseToken()->authenticate();

        $data           = User::select('id', 'uid', 'name', 'name_kh', 'phone', 'email', 'creator_id', 'created_at', 'address')
        ->with([
            'creator:id,name',
        ])
        ->with(array('customer'=>function($query){
            $query->select('*')->with([
                'province:id,name',
                'district:id,name',
                'refferal',
                'sponsor',
                'subscriptions',
                'roles'   
            ]);
        }))
      ;
      $subscription           = MemberSubscription::select('*')
      ->addSelect(DB::raw("DATEDIFF(expired_at, NOW()) AS expired_countdown"))
      ->where([
          'member_id' => $user->member->id,
      ])
      ->with([
          'subscription',
      ])
      ->orderBy('id', 'desc')
      ->first();

        // return $user;
        // Want to check if customer is valid
        if($request->username){
            $customer = $data->where(function($query) use ($request){
                $query->where('phone', $request->username)->orWhere('email', $request->username)->orWhere('uid', $request->username); 
            })
            ->first(); 
            return response()->json($customer, 200);
        }
        
        $data       = $data->where('creator_id', $user->id)
        ->orWhereHas('customer', function($query) use ($user){
            $query->where('refferal_id', $user->member->id); 
        })->whereNotIn('id', [$user->id]); 
       
        // Key search API
        $limit      =   intval(isset($_GET['limit']) ? $_GET['limit'] : 10);
        $key       =   isset($_GET['key']) ? $_GET['key'] : "";

        if ($key != "") {
            $data = $data->where(function ($query) use ($key) {
                $query->where('name', 'like', '%' . $key . '%');
            });
            $appends['key'] = $key;
        }

        // Key search Date API
        $from = isset($_GET['from']) ? $_GET['from'] : "";
        $to = isset($_GET['to']) ? $_GET['to'] : "";
        if (isValidDate($from)) {
            if (isValidDate($to)) {

                $from .= " 00:00:00";
                $to .= " 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }

        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json(
           [
                'subscription'   => $subscription,
                'data'           =>  $data,
            ], 200);
 
    }

    function create(Request $request){
       
        // Check validation
        $this->validate($request, [
            'refferal'  => 'required', 
            'name'      => 'required', 
           
            'phone'     =>  [
                'required',  
                 Rule::unique('user', 'phone')
            ],
            
            'province_id' => 'required|exists:province,id',
            'district_id' => 'required|exists:district,id',
        ], 
        [
            
            'name.required' => 'សូម​បញ្ចូល​ឈ្មោះ​របស់​លោកអ្នក',
            'phone.required' => 'សូមបញ្ចូលលេខទូរស័ព្ទ។',
            'phone.unique' => 'លេខទូរស័ព្ទបានបង្កើតរួចហើយ។ សូមជ្រើសរើសមួយផ្សេងទៀត', 
                
            'province_id.required' => 'សូមជ្រើសរើសខេត្ត។',
            'province_id.exists' =>  'សូមជ្រើសរើសខេត្តដែលមានសុពលភាព។',

            'district_id.required' => 'សូមជ្រើសរើសស្រុក។', 
            'district_id.exists' => 'សូមជ្រើសរើសស្រុកដែលមានសុពលភាព។',


        ]);
        
        // Current Authenicated Distributor
        $creator        = JWTAuth::parseToken()->authenticate();
        $refferal       = User::where('uid', $request->refferal)->with('member')->first(); 
        $sponsor        = User::where('uid', $request->sponsor)->with('member')->first(); 
        
        if($sponsor){
            if($refferal){
                DB::beginTransaction();

                try {
                    //====================================>> Create New user
                    $user = new User();
                    $user->type_id              = 3;
                    $user->uid                  = Account::generateUid();
                    $user->name_kh              = $request->input('name_kh');
                    $user->name                 = $request->input('name');
                    $user->phone                = $request->input('phone');
                    $user->is_phone_verified    = 1;
                    $user->email                = $request->input('email');
                    $user->address              = $request->input('address');
                    // $user->password          = bcrypt(rand(10000000, 999999));
                    $user->password             = bcrypt('123456'); // TODO
                    $user->creator_id           = $creator->id; 
                    
                    $user->save();
                    $customer = new Customer; 
                    $customer->sponsor_id   =  $sponsor->customer->id;  
                    $customer->refferal_id  =  $refferal->customer->id;  
                    $customer->user_id      =  $user->id; 
                    // $customer->subscription_id=4;
                    $customer->province_id  =  $request->input('province_id');
                    $customer->district_id  =  $request->input('district_id');
                    $customer->card         = $request->input('card');
                    $customer->address      = $request->input('address');
                             
                        //Need to create folder before storing images
                        $image = FileUpload::uploadImage($request, 'image', ['uploads', '/idcard', '/'.uniqid(), '/images'], [['xs', 200, 200]]);
                        if($image != ""){
                            $customer->image = $image;
                        }

                    $customer->save(); 

                  
                    DB::commit();
                } catch (\Throwable $e) {
                    DB::rollback();
                    throw $e;
                }         
              
                return response()->json([
                    'status' => 'success', 
                    'message' => 'អតិថិជនថ្មីត្រូវបានបង្កើត។', 
                    'user' => $user, 
                    'customer' => $customer
                ], 200);
            }else{
                return response()->json(['message' => 'Invalid refferal'], 400);
            }
        }else{
            return response()->json(['message' => 'Invalid sponsor'], 400);
        }
    }

    function provinces(Request $request){
       
        $data           = Province::select('id', 'name')
        ->with([
            'districts:id,province_id,name'
        ])
        ->get();
        return response()->json($data, 200);

    }
    
    function getAccount(Request $req){

        $member = Customer::select('id', 'user_id', 'branch_id', 'depot_id', 'branch_owner_id', 'role_id', 'subscription_id')
        ->whereHas('user', function($query) use ($req){
            $query->where('uid', $req->uid); 
        })
        ->with([
            'user:id,name_kh,name,uid,email,phone', 
            'branch', 
            'depot', 
            'roles',
        ])
        ->first();

        if($member){
            return $member; 
        }else{
            return []; 
        }

   
    }

    function changeSponsor(Request $request, $id=0){
        $data = User::with('member')->findOrFail($id);
        
        $old_user  = User::select('id', 'name')
        ->with('member')
        ->where('uid', $request->old_ref)
        ->first();
        //return $data->member->sponsor_id;
        $old_sponsor = $old_user->member->id;
        //return $old_sponsor;
        if($data){
            if($data->member){
                if($data->member->sponsor_id == $old_sponsor){
                   
                    // $data->uid = $request->new_ref;
                    // $data->save();
                    $new_user  = User::select('id', 'name')
                    ->with('member')
                    ->where('uid', $request->new_ref)
                    ->whereHas('member', function($query){
                        $query->whereNotNull('subscription_id');
                    })
                    ->first();

                    if(!$new_user){
                        return response()->json([
                            'status' => 'fail',
                            'message' => 'អ្នកឧបត្ថម្ភថ្មីត្រូវតែជាសមាជិកឡេីងទៅ', 
                        
                        ], 400);
                    }
    
                    $new_sponsor = $new_user->member->id;
                    $old_member = $data->member;
                    $old_member->sponsor_id = $new_sponsor;
                    $old_member->save();
    
                    return response()->json([
                        'status' => 'success',
                        'message' => 'ទិន្នន័យត្រូវបានធ្វើបច្ចុប្បន្នភាព។', 
                        'data' => $data
                    ], 200);
                }else{
                    return response()->json([
                        'status' => 'fail',
                        'message' => 'អ្នកឧបត្ថម្ភមិនត្រឺមត្រូវ', 
                    
                    ], 400);
                }
            }else{
                return response()->json([
                    'status' => 'fail',
                    'message' => 'អ្នកឧបត្ថម្ភមិនត្រឺមត្រូវ'
                ], 400);
            }

            
        }else{
            return response()->json([
                'status' => 'fail',
                'message' => 'កំណត់ត្រាអតិថិជនមិនត្រឹមត្រូវ។', 
            
            ], 400);
        }
        return $data;

        



    }
 
} 
