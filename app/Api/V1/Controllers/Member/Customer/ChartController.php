<?php

namespace App\Api\V1\Controllers\Member\Customer;

use Illuminate\Http\Request;

use App\Api\V1\Controllers\ApiController;
use App\Model\Member\Main as Member;
use App\Model\Member\Node as Node;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\MGTB\Network;

class ChartController extends ApiController
{
    use Helpers;
    
    function rootNode(){
       
        return response()->json(Network::rootNode(), 200);
    }
    
    function getNodes(){
       
        return response()->json(Network::getNodes(), 200);
    }

}
