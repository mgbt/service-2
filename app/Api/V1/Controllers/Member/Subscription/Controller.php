<?php

namespace App\Api\V1\Controllers\Member\Subscription;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Carbon\Carbon;

use App\Api\V1\Controllers\ApiController;

use App\Model\Subscription\Main as Subscription;
use App\Model\Member\Subscription as MemberSubscription;



class Controller extends ApiController
{
    use Helpers;
   
    function listing(Request $request){
       
        $data           = Subscription::select('id', 'name', 'requirement', 'unit_price', 'min_purchase', 'role')
        ->where('id', '>', 1)
        ->get();
        return response()->json($data, 200);
    }

    function mySubscriptions(Request $request){
        $user         = JWTAuth::parseToken()->authenticate();

        $request           = MemberSubscription::select('*')->where([
            'member_id' => $user->member->id, 
            'status_id' => 1 //Pending
        ])
        ->with([
            'subscription'
        ])
        ->orderBy('id', 'desc')
        ->first();

        $subscription           = MemberSubscription::select('*')->where([
            'member_id' => $user->member->id, 
        ])
        ->where('status_id', '<>', 1)
        ->with([
            'subscription'
        ])
        ->orderBy('id', 'desc')
        ->first();

        $histories           = MemberSubscription::select('*')->where([
            'member_id' => $user->member->id, 
        ])
        //->where('status_id', '<>', 1)
        ->with([
            'subscription', 
            'status'
        ])
        ->orderBy('id', 'desc')
        ->get();

        return response()->json([
            'current' => $subscription, 
            'histories' => $histories, 
            'request' =>  $request
        ], 200);
    }

    function subscrbe(Request $request){

        $this->validate($request, [
            'subscription' => 'required|exists:subscription,id'
        ]);

        $user         = JWTAuth::parseToken()->authenticate();

        $subscription                   = new MemberSubscription(); 
        $subscription->subscription_id  = $request->subscription; 
        $subscription->member_id        = $user->member->id; 
        $subscription->status_id        = 1; //Pending 
        $subscription->save(); 
        

        return response()->json([
            'message' => 'ជាវដោយជោគជ័យ!', 
            'subscription' => $subscription, 
        ], 200);
       
    }

    
}
