<?php

namespace App\Api\V1\Controllers\Member\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Package\Product;
use App\Model\Package\Main as Package;
use App\Model\Package\PackageProduct;
use App\Model\Stock\Detail as Stock;
use App\Model\Event\Request as VenueRequest;
use Dingo\Api\Routing\Helpers;
use JWTAuth;


class Controller extends ApiController
{
    use Helpers;
    function listing(Request $req){
        
        $data = Product::select('*')
        ->with([
           'category',
            'info' 
        ])
        ->whereHas('info', function($query){
            $query->whereHas('package', function($query){
                $query->whereHas('prices'); 
            });
        });
        ;
       
        // Search by Key
        if($req->key && $req->key != "") {
            $data = $data->where(function ($query) use ($req) {
                $query->where('kh_name', 'like', '%' . $req->key . '%')
                ->orWhere('en_name', 'like', '%' . $req->key . '%')
                ;
            });
        }
        $category       =   isset($_GET['category'])?$_GET['category']:0;
        if( $category != 0 ){
            $data = $data->where('category_id', $category);
            $appends['category'] = $category;
        }
        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }

    function view($id = 0){

        $data   = Product::select('*')
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
        
    }
    
    function stocks(Request $request){
        $user       = JWTAuth::parseToken()->authenticate();
        $data       = Stock::select('id', 'package_id', 'qty')
        ->with([
            'package:id,en_name,selling_price', 
            'package.products'
        ])
        ->get(); 
        return response()->json($data, 200);
 
    }


   
}
