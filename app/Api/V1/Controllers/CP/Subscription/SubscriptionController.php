<?php

namespace App\Api\V1\Controllers\CP\Subscription;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use App\Model\Member\Main as Subscription;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

class SubscriptionController extends ApiController
{
    use Helpers;
    function listing(Request $req) {

        $data = Subscription::select('*')
        ->with([
            'details', 
            'member'
        ])
        ->withCount([ 
            'members as n_of_members',
        ]);

         // ==============================>> Date Range
         if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        // ==============================>> Pending Actions
        if($req->member && $req->member != '' ){
            $data = $data->where('member_id', $req->member);
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }

}
