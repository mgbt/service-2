<?php

namespace App\Api\V1\Controllers\CP\Order;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use App\Model\Order\Main as Order;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

class OrderController extends ApiController
{
    use Helpers;
    function listing(Request $req) {

        $data = Order::select('*')
        ->with([
            'details', 
            'buyer',
            'seller'
        ])
        ;

         // ==============================>> Keyword
         if( $req->key && $req->key !="" ){
            $data = $data->whereHas('member', function($q) use($req){
                $q->whereHas('user', function($q) use($req){
                    $q->where('name', 'like', $req->key)->orWhere('phone', 'like', $req->key)->orWhere('email', 'like',  $req->key)->orWhere('uid', 'like', $req->key);
                });
            });
        }

          // ==============================>> Order ID  
          if( $req->receipt_number && $req->receipt_number !="" ){
            $data = $data->where('receipt_number', $req->receipt_number);
        }

         // ==============================>> Date Range
         if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        // ==============================>> Pending Actions

        if( $req->order_id && $req->order_id !="" ){
            $data = $data->where('id', $req->order_id);
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }

}
