<?php

namespace App\Api\V1\Controllers\CP\Member;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Api\V1\Controllers\ApiController;
use App\Model\Member\Main as Member;
use App\Model\Member\DepotRequest;
use App\Model\Member\Subscription as MemberSubscription;

class RequestController extends ApiController
{
    use Helpers;
    function listing(Request $req) {

        $data = DepotRequest::select('*')
        //->where('action_by', null)
        ->with([
            'branch', 
            'district', 
            'depot'
        ])

        ; 

          // ==============================>> Date Range
        if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }

    function action(Request $request, $id = 0){

        $this->validate($request, [
            'status' => 'required'
        ]);

        $memberRequest = DepotRequest::where('action_by', null)
        ->find($id); 

        if($memberRequest){
            $user = JWTAuth::parseToken()->authenticate(); 

            if($request->status == 'accept'){

                $member = Member::select('id', 'user_id', 'branch_id', 'depot_id', 'subscription_id')
                ->find($memberRequest->depot_id);

                if($member){

                    $member->branch_owner_id   = $memberRequest->branch_id; 
                    $member->depot_id         = $memberRequest->district_id; 
                    //$member->structure_activated_at = now(); 
                    // Delete branch account; 
                    $member->branch_id = null; 
                    $member->save(); 

                    $memberRequest->accepted_at = now(); 
                    $memberRequest->action_by = $user->id; 
                    $memberRequest->save(); 

                    return response()->json([
                        'status' => 'success', 
                        'message' => 'អ្នកបានទទួលយកសំណើនេះដោយជោគជ័យ។'
                    ], 200); 
                }
                
                

            }elseif($request->status == 'reject'){

                $memberRequest->rejected_at = now(); 
                $memberRequest->action_by = $user->id; 
                $memberRequest->save(); 

                return response()->json([
                    'status' => 'fail', 
                    'message' => 'អ្នកបានបដិសេធសំណើនេះដោយជោគជ័យ។'
                ], 200); 

            }else{
                return response()->json([
                    'status' => 'fail', 
                    'message' => 'ស្ថានភាពមិនត្រឹមត្រូវ'
                ], 404); 
            }
        }else{
            return response()->json([
                'status' => 'fail', 
                'message' => 'កំណត់ត្រាមិនត្រឹមត្រូវ'
            ], 404); 
        }


    }

    function ConvertToMember(Request $request, $id = 0){

        $user = JWTAuth::parseToken()->authenticate(); 

        $member = Member::select('*')
        ->where('subscription_id', null)
        ->find($id);

        if($member){

           
            $member->subscription_id  = 4; // Member
            $member->save(); 

            // Subscription History
            $subscription                   = new MemberSubscription; 
            $subscription->member_id        = $member->id; 
            $subscription->status_id        = 2; //Active
            $subscription->subscription_id  = 4; // Member
            $subscription->activated_at     = now(); 
            //$subscription->expired_at       = date('Y-m-d 23:59:59', strtotime('+30 days', strtotime(Date('Y-m-d')))); 
            $subscription->save();

            // TODO: send notification to customer. 

            return response()->json([
                'status' => 'success', 
                'message' => 'អ្នកបានទទួលយកសំណើនេះដោយជោគជ័យ។'
            ], 200); 
        }else{
            return response()->json([
                'status' => 'fail', 
                'message' => 'Invalid member.'
            ], 400);
        }
       

    }


   

}
