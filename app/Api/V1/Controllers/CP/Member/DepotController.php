<?php

namespace App\Api\V1\Controllers\CP\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Member\Depot as Depot;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\Model\Member\Main as Member;

class DepotController extends ApiController
{
    use Helpers;
    function listing(Request $req) {
        $data = Member::select('id', 'user_id', 'subscription_id', 'district_id', 'province_id', 'created_at')
        ->with([
            'user', 
            'subscription',
            'district',
            'province',
            'activeSubscription'
        ])
        ->withCount([
            'orders as n_of_orders', 
            'purchases as n_of_purchases'
        ])
        ->whereIn('subscription_id',[
            5
        ]); 
        
        // ==============================>> Location
        if($req->province){
            $data = $data->where('province_id', $req->province); 
        }

        if($req->district){
            $data = $data->where('district_id', $req->district); 
        }

        // ==============================>> Subscription
        if($req->subscription){
            $data = $data->where('subscription_id', $req->subscription); 
        }

        // ==============================>> Subscription
        if($req->role){
            $data = $data->whereHas('subscription', function($query) use ($req){
                $query->where('role_id', $req->role); 
            }); 
        }

        // ==============================>> Keyword
        if( $req->key && $req->key !="" ){
            $data = $data->whereHas('user', function($q) use($req){
                $q->where('name', 'like', $req->key)->orWhere('phone', 'like', $req->key)->orWhere('email', 'like',  $req->key)->orWhere('uid', 'like', $req->key);
            });
        }

        // ==============================>> Date Range
        if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }
    
    // function create(Request $req)
    // {
       
    //     $this->validate($req, [
    //         'name'              => 'required|max:20',
            
    //     ], [
    //         'name.required'         => 'Plese enter store name.',
    //         'name.max'              => 'Store name must not be more then 20 chacters.',

    //     ]);

    //     $data                       = new member();
    //     $data->name                 = $req->name;
    //     $data->save();

    //     return response()->json([
    //         'status' => 'success',
    //         'message' => 'Succefully created',
    //         'data' => $data,
    //     ], 200);
    // }
    
   

}
