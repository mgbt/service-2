<?php

namespace App\Api\V1\Controllers\CP\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\Api\V1\Controllers\ApiController;

use App\Model\User\Main as User;
use App\Model\Member\Main as Member;
use App\Model\Member\Subscription as MemberSubscription;
use App\Model\Setup\Province;
use App\Model\Setup\District;
use App\Model\Subscription\Main as Subscription; 
use App\MGTB\Account;

class AccountInformationController extends ApiController
{
    use Helpers;
    function info($Id = 0 ){
        
        $member     = Member::select('*')
        ->with([
            'subscription',
            'subscriptions',
            'activeSubscription',
            'user',
            'branchOwner',
            'depot',
            'roles',
            'branch'
           
            
        ])
        ->where('id', $Id)
        ->get();
        return response()->json([
            'member'   => $member, 
        ], 200);
    }
    
}
