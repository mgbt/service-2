<?php

namespace App\Api\V1\Controllers\CP\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Carbon\Carbon;
use App\Api\V1\Controllers\ApiController;

use App\Model\User\Main as User;
use App\Model\Member\Main as Member;
use App\Model\Setup\Province;
use App\Model\Setup\District;



class AccountController extends ApiController
{
    use Helpers;
   

    function create(Request $request){

        $started = Carbon::parse($request->contract_started);
        $expired = Carbon::parse($request->contract_expired);
   
        if($expired->greaterThan($started)){
            if($request->uid && $request->uid != ""){   // Existing user
                // Check if uid is correct
                $user = User::where([
                'uid' => $request->uid
                ])
                ->whereHas('member')
                ->first(); 
                
                if($user){

                    // Check condition on account Type
                    if($request->account_type == 'branch'){
                        //Check if branch is valid
                        $province = Province::find($request->branch); 
                        if(!$province){
                            return response()->json([
                                'message' => 'សូមជ្រើសរើសសាខាខេត្តអោយបានត្រឹមត្រូវ'
                            ], 400); 
                        }
                        // Check if user already had branch account
                        if($user->member->branch_id && $user->member->branch_id == $request->branch){
                            return response()->json([
                                'message' => 'អ្នកប្រើប្រាស់មានគណនីសាខារួចហើយនៅក្នុងខេត្តនេះ។'
                            ], 400); 
                        }

                        $user->member->branch_id        = $province->id; 
                        $user->member->contract_started = $started->format('Y-m-d'); 
                        $user->member->contract_expired = $expired->format('Y-m-d'); 

                        // Delete depot account; 
                        $user->member->depot_id = null; 
                        $user->member->save(); 
                 
                    }elseif($request->account_type == 'depot'){
                        $district = District::where([
                            'province_id' => $request->branch, 
                            'id' => $request->depot
                        ])->first(); 
        
                        if(!$district){
                            return response()->json([
                                'message' => 'សូមជ្រើសរើស ដ៉េប៉ូ អោយបានត្រឹមត្រូវ'
                            ], 400); 
                        }

                        // Check if user already had depot account
                        if($user->member->depot_id && $user->member->depot_id == $request->depot ){
                            return response()->json([
                                'message' => 'អ្នកប្រើប្រាស់មានគណនីដេប៉ូរួចហើយ។'
                            ], 400); 
                        }
                        // Check branch owner account
                        $branch = Member::where([
                            'id' => $request->branch_owner_id, 
                            'branch_id' => $request->branch
                        ])->first(); 
                        
                        if(!$branch){
                            return response()->json([
                                'message' => 'សាខាដែលបានដាក់ជូនមិនមានសុពលភាពទេ។'
                            ], 400); 
                        }

                        $user->member->branch_owner_id   = $request->branch_owner_id; 
                        $user->member->depot_id         = $district->id; 

                        $user->member->contract_started = $started->format('Y-m-d'); 
                        $user->member->contract_expired = $expired->format('Y-m-d'); 
                        
                        // Delete depot account; 
                        $user->member->branch_id = null; 
                        $user->member->save(); 

                    }else{
                        return response()->json(['message'=> 'សូមជ្រើសរើសប្រភេទគណនី។'], 400);
                    }

                    return response()->json(['message'=> 'បន្ថែមដោយជោគជ័យ'], 200);


                }else{
                    return response()->json(['message'=> 'លេខសម្គាល់អ្នកប្រើមិនត្រឹមត្រូវ'], 400);
                }

            }else{
                return response()->json(['message'=> 'លេខសម្គាល់អ្នកប្រើមិនត្រឹមត្រូវ'], 400);
            }
        }else{
            return response()->json(['message'=> 'Invalid date'], 400);
        }
    }
 
    function packageInfo(Request $request){
        return response()->json([
            'provinces' => Province::select('id', 'name')
                ->with([
                   'districts:id,province_id,name', 
                    'branches'
                ])
                ->get(), 
            //'subscriptions' => Subscription::select('id', 'name')
            //->whereIn('id', [5, 6])
            //->get()

        ], 200);
   
    }
    function getAccount(Request $req){

        $member = Member::select('id', 'user_id', 'branch_id', 'depot_id', 'branch_owner_id')
        ->whereHas('user', function($query) use ($req){
            $query->where('uid', $req->uid); 
        })
        ->with([
            'user:id,name,uid,email,phone', 
            'branch', 
            'depot'
        ])
        ->first();

        if($member){
            return $member; 
        }else{
            return []; 
        }

   
    }


    
}
