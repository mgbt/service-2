<?php

namespace App\Api\V1\Controllers\CP\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Api\V1\Controllers\ApiController;
use App\Model\Member\BranchPackage;
use App\Model\Package\Product;
use App\Model\Package\Main as Package;
use App\Model\Member\Main as Member;

use App\MGTB\Stock; 
// Import Resource
use App\Api\V1\Resources\Package\PackageResource;
use App\Api\V1\Resources\Package\PackagesCollection;

class PackageController extends ApiController
{
    use Helpers;

  
    function listing($memberId = 0 ){


        $data = Package::select('id', 'sku', 'en_name', 'en_name', 'kh_name', 'image', 'is_single_product')
        ->whereHas('branches', function($query) use ($memberId) {
            $query->where('branch_id', $memberId);
        })
        ->with([
            //'info', 
            'products'
        ])
        ->get();
       
        $data = new PackagesCollection($data);
        return response()->json($data, 200);
    }

    // Add or Remove
    function action($branchId = 0,$packageId = 0){
        //return $packageId;
       
        //Check if Package is valid

        $package = Package::where([
            
            'id' => $packageId, 
            //'is_single_product' => 0

        ])->first();

        if($package){
             //Check if Package really existed in the package

             $check = BranchPackage::where([

                'branch_id' => $branchId, 
                'package_id' => $packageId

            ])->first(); 

            if($check){

                //Remove
                $check->delete(); 

                // Update Number of Units
                //Stock::updatePackageUnit($check->package); 

                return response()->json([
                    'status' => 'success',
                    'message' => 'កញ្ចប់ត្រូវបានយកចេញ',
                ], 200);
            }else{

                $data = new BranchPackage; 
                $data->branch_id =  $branchId; 
                $data->package_id =  $packageId; 
                $data->save(); 

               

                return response()->json([
                    'message' => 'កញ្ចប់ត្រូវបានបន្ថែម',
                    'data' => $data
                ], 200);
            }

        }else{
            return response()->json([
                'message' => 'កញ្ចប់មិនត្រឹមត្រូវ',
            ], 400);
        }

      
    }

    function availablePackages($memberId = 0){

        $branch = Member::whereHas('branch')->find($memberId); 
        if($branch){
            $exceptBranches = [$branch->id]; 
            $otherBranches = Member::select('id')->where('branch_id', $branch->branch_id)->get(); 
            foreach($otherBranches as $anotherBranch){
                $exceptBranches[] = $anotherBranch->id; 
            }
            
            $data = Package::select('id', 'sku', 'en_name', 'en_name', 'kh_name', 'image', 'is_single_product')
            ->whereDoesntHave('branches', function($query) use ($exceptBranches) {
                $query->whereIn('branch_id', $exceptBranches); 
            })
            ->whereHas('products')
            ->with([
                'info'
            ])
            ->get();  

            $data = new PackagesCollection($data);
            return response()->json($data, 200);
        }

        return response()->json([], 200);

      
    }

}
