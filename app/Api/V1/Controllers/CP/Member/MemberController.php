<?php

namespace App\Api\V1\Controllers\CP\Member;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\User\Main as User;
use App\Model\Member\Main as Member;
use App\Model\Setup\Province;
use App\Model\Setup\District;
use App\Model\Setup\Role;
use App\Model\Subscription\Main as Subscription;

// Import Resource
use App\Api\V1\Resources\Product\ProductResource;
use App\Api\V1\Resources\Product\ProductCollection;

class MemberController extends ApiController
{
    use Helpers;
    function listing(Request $req) {
      
        $data = Member::select('id', 'user_id', 'sponsor_id','card', 'subscription_id', 'district_id', 'province_id', 'branch_id', 'depot_id', 'contract_started', 'contract_expired', 'is_structure_active', 'structure_activated_at', 'created_at')
        ->with([
            'user', 
            'district',
            'province',
            'depot',
            'branch',
            // 'subscription',
            'activeSubscription'
        ])
        ->withCount([
            'orders as n_of_orders', 
            'purchases as n_of_purchases'
        ])
        ; 
        
        // ==============================>> Location
        if($req->province){
            $data = $data->where('province_id', $req->province); 
        }

        if($req->district){
            $data = $data->where('district_id', $req->district); 
        }

        // ==============================>> Subscription
        if($req->subscription){
            $data = $data->where('subscription_id', $req->subscription); 
        }

        // ==============================>> Role
        if($req->role){
            if($req->role == 4){
                $data = $data->where('branch_id', '<>', null); 
            }elseif($req->role == 3){
                $data = $data->where('depot_id', '<>', null); 
            }elseif($req->role == 2){
                $data = $data->where('subscription_id', '<>', null);  
            }elseif($req->role == 1){
                $data = $data->where('subscription_id', null);  
            }
        }

        // ==============================>> Keyword
        if( $req->key && $req->key !="" ){
            $data = $data->whereHas('user', function($q) use($req){
                $q->where('name', 'like', $req->key)->orWhere('phone', 'like', $req->key)->orWhere('email', 'like',  $req->key)->orWhere('uid', 'like', $req->key);
            });
        }

        // ==============================>> Date Range
        if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        // ==============================>> Refferal
        if($req->refferal){
            $data = $data->where('sponsor_id', $req->refferal); 
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }

    function view($id = 0){

        $data   = Member::select('*')
        ->with([
            'user',
            'province',
            'district',
            'refferal',
            'sponsor'


        ])
        ->find($id); 

        if($data){
            $data = new ProductResource($data);
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }

        // if( $req->order_id && $req->order_id !="" ){
        //     $data = $data->where('id', $req->order_id);
        // }
        
    }

    function update(Request $request, $id=0){
     
        $admin     = JWTAuth::parseToken()->authenticate();
        $member    = Member::select('*')->find($id);
       //return $id;
        if($member){

            $user = $member->user; 

            $this->validate($request,[

                'name' => 'required|max:150',
                'phone' =>  [
                                'required', 
                                'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                Rule::unique('user', 'phone')->ignore($member->user->id)
                            ],
                'email'=>   [
                                'sometimes', 
                                'required', 
                                'email', 
                                'max:100', 
                                Rule::unique('user', 'email')->ignore($member->user->id)
                            ],
                        ],[
                'name.required'       =>   'Please enter your name.',
                'name.max'            =>   'Name has been digit  to 60.',

                'phone.required'      =>   'Please enter your phone number.',
                'phone.regex'         =>   'Phone number is required.',

                'email.required'      =>   'Please enter your email.',
                'email.email'         =>   'Email is required.',
                'email.max'           =>  'email has been digit  to 100.',
              
            ]);

            $user->name_kh  = $request->name_kh;
            $user->name     = $request->name;
            $user->phone    = $request->phone;
            $user->email    = $request->email;
            $user->address  = $request->address;
            $user->save();

            $member->province_id  =  $request->input('province_id');
            $member->district_id  =  $request->input('district_id');
            $member->card         = $request->input('card');

            $last = Member::select('id')->orderBy('id', 'DESC')->first();
            $id = 0;
            if($last){
                $id = $last->id+1;
            }

            $image = FileUpload::uploadImage($request, 'image', ['/uploads', '/idcard', '/'.$id, '/images'], [['xs', 200, 200]]);
            if($image != ""){
                $member->image = $image;
            }
            $image = FileUpload::resize($request, 'image', ['/uploads', '/idcard', '/'.$id, '/images'], [['xs', 200, 200]]);
            if($image != ""){
                $member->image = $image;
            }
            if($image != ""){
                $member->image = $image;
            }
            $member->save();
                            
            return response()->json([
                'status' => 'success',
                'message' => 'ទិន្នន័យត្រូវបានធ្វើបច្ចុប្បន្នភាព។', 
                'member' => $member
            ], 200);

        }else{
            return response()->json([
                'status' => 'fail',
                'message' => 'កំណត់ត្រាអតិថិជនមិនត្រឹមត្រូវ។', 
            
            ], 400);
        }
    }

    // function delete($id=0){
    //     $data = Main::find($id);
    //     if(!$data){
    //         return response()->json([
    //             'message' => 'រកមិនឃើញទិន្នន័យ', 
    //         ], 404);
    //     }
    //     $data->delete();

    //     $User = User::find($data->user_id)->delete();

    //     return response()->json([
    //         'status' => 'success',
    //         'message' => 'ទិន្នន័យត្រូវបានលុបចោល។ ',
    //     ], 200);
    // }

    function fillter(){

        $provinces = Province::select('id', 'name')
        ->with([
            'districts:id,province_id,name'
        ])
        ->get();
        
        $roles = Role::select('id', 'name')
        ->get();

        $subscriptions = Subscription::select('id', 'name')
        ->get();

        return response()->json([
            'provinces' => $provinces,
            'roles' => $roles,
            'subscriptions' => $subscriptions,
        ], 200);
        
    }

    function updatePassword(Request $request, $id=0){
        $this->validate($request, [
            'password' => 'required|min:6|max:60',
        ]);

        $user = User::find($id); 
        if($user){
           
            $user->password = bcrypt($request->input('password'));
            $user->save();

            return response()->json([
                'status' => 'success',
                'message' => 'ពាក្យសម្ងាត់ត្រូវបានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ។', 
               
            ], 200);
        }else{
            return response()->json([
                'status' => 'fial',
                'message' => 'សមាជិកមិនត្រឹមត្រូវ', 
               
            ], 404);
        }
        
        
    }
    function provinces(Request $request){
       
        $data           = Province::select('id', 'name')
        ->with([
            'districts:id,province_id,name'
        ])
        ->get();
        return response()->json($data, 200);

    }
    function districts(Request $request,$id=0){
       
        $data           = District::select('id', 'name')
        ->where('province_id',$id)
        ->get();

        return response()->json($data, 200);

    }

    function changeSponsor(Request $request, $id=0){
        $data = User::with('member')->findOrFail($id);
        
        $old_user  = User::select('id', 'name')
        ->with('member')
        ->where('uid', $request->old_ref)
        ->first();
        // return $old_user;
        $old_sponsor = $old_user->member->sponsor_id;
        
        if($data){
            if($data->member->sponsor_id == $old_sponsor){
                // $data->uid = $request->new_ref;
                // $data->save();
                $new_user  = User::select('id', 'name')
                ->with('member')
                ->where('uid', $request->new_ref)
                ->first();

                $new_sponsor = $new_user->member->id;
                $old_member = $data->member;
                $old_member->sponsor_id = $new_sponsor;
                $old_member->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'ទិន្នន័យត្រូវបានធ្វើបច្ចុប្បន្នភាព។', 
                    'data' => $data
                ], 200);
            }else{
                return response()->json([
                    'status' => 'fail',
                    'message' => 'អ្នកឧបត្ថម្ភមិនត្រឺមត្រូវ', 
                
                ], 400);
            }
        }else{
            return response()->json([
                'status' => 'fail',
                'message' => 'កំណត់ត្រាអតិថិជនមិនត្រឹមត្រូវ។', 
            
            ], 400);
        }
        return $data;

        



    }
 

}
