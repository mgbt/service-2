<?php

namespace App\Api\V1\Controllers\CP\ProductCategory;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Package\Product;
use App\Model\Package\Main as Package;
use App\Model\Package\PackageProduct;
use App\Model\Event\Request as VenueRequest;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

// // Import Resource
// use App\Api\V1\Resources\Product\ProductResource;
// use App\Api\V1\Resources\Product\ProductsCollection;
// use App\Api\V1\Resources\Package\PackageResource;
// use App\Api\V1\Resources\Package\PackagesCollection;

class Controller extends ApiController
{
    use Helpers;
  
    function listing() {
        $data = Package::select('*')
        ->with([
            'category:id,name', 
            'products'
        ])
        ->where('is_single_product', 1)
        ;

        $limit      =   intval(isset($_GET['limit']) ? $_GET['limit'] : 10);
        $key       =   isset($_GET['key']) ? $_GET['key'] : "";

        if ($key != "") {
            $data = $data->where(function ($query) use ($key) {
                $query->where('name', 'like', '%' . $key . '%');
            });
        }

        $single       =   isset($_GET['single']) ? $_GET['single'] : 0;
       

        $from = isset($_GET['from']) ? $_GET['from'] : "";
        $to = isset($_GET['to']) ? $_GET['to'] : "";
        if (isValidDate($from)) {
            if (isValidDate($to)) {

                $from .= " 00:00:00";
                $to .= " 23:59:59";
                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }

        $data = $data->orderBy('id', 'desc')->paginate($limit);
        
        return $data;
    }

    function view($id = 0){
        
       
        $data   = Package::select('*')
        ->with(
            'category:id,name',
            'products'
            
            )
        ->where('is_single_product', 1)
        ->find($id);

        if($data){
            $data = new ProductResource($data);
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
        
    }

    function create(Request $request){

        $admin_id = JWTAuth::parseToken()->authenticate()->id;
      
        $this->validate($request, [
        
        ]);


        /** Save Product */
        $data                       = new Product();
        $data->category_id             = $request->input('category_id');
        $data->description          = $request->input('description');
      
        $data->save();


        /** Save Package */
        $package                        = new Package();
        $package->name                  = $request->input('name');
        $package->cost_price            = $request->input('cost_price');
        $package->selling_price         = $request->input('selling_price');
        $package->branch_discount       = $request->input('branch_discount');
        $package->store_discount        = $request->input('store_discount');
        $package->customer_discount     = $request->input('customer_discount');
        $package->pv                    = $request->input('pv');
        $package->is_active             = $request->input('is_active');
        $package->is_single_product     = 1;
      
        $last = Product::select('id')->orderBy('id', 'DESC')->first();
        $id = 0;
        if($last){
            $id = $last->id+1;
        }
          
        //Need to create folder before storing images
        $image = FileUpload::uploadImage($request, 'image', ['/uploads', '/products', '/'.$id, '/image'], [['xs', 200, 200]]);
        if($image != ""){
            $data->image = $image;
        }
        $image = FileUpload::resize($request, 'image', ['/uploads', '/products', '/'.$id, '/image'], [['xs', 200, 200]]);
        if($image != ""){
            $data->image = $image;
        }
        if($image != ""){
            $package->image = $image;
        }
        $package->save();

        /** Save Package Product */
        $package_product                    = new PackageProduct();
        $package_product->product_id        = $package->id; 
        $package_product->package_id        = $package->id;
        $package_product->qty               = 1;
      
        $package_product->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ', 
            'data' => $package, 
        ], 200);
    }

    function update(Request $request, $id=0){
        $this->validate($request, [
            'name'                 => 'required|max:60',
            'cost_price'           => 'required|numeric',
            'selling_price'        => 'required|numeric',
          
        ]);
        
        //========================================================>>>> Start to update
        $data                       = Product::findOrFail($id);
        $data->category_id          = $request->input('category_id');
        $data->description          = $request->input('description');
        //Need to create folder before storing images
        
        $data->save();

        
        /** Find Package Id */
        $product_package = PackageProduct::where('product_id', $id)->first();

        $package = Package::findOrFail($product_package->package_id);
        $package->name                      = $request->input('name');
        $package->cost_price                = $request->input('cost_price');
        $package->selling_price             = $request->input('selling_price');
        $package->branch_discount           = $request->input('branch_discount');
        $package->store_discount            = $request->input('store_discount');
        $package->customer_discount         = $request->input('customer_discount');
        $package->description               = $request->input('description');
        $package->is_single_product         = 1;

          //Need to create folder before storing images
        $image = FileUpload::uploadImage($request, 'image', ['/uploads', '/products', '/'.$id, '/image'], [['xs', 200, 200]]);
        if($image != ""){
            $data->image = $image;
        }
        $image = FileUpload::resize($request, 'image', ['/uploads', '/products', '/'.$id, '/image'], [['xs', 200, 200]]);
        if($image != ""){
            $data->image = $image;
        }
      
        $package->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ', 
            'data' => $data
        ], 200);
    }
    
    function delete($id=0, $package_id){
        $product_package = PackageProduct::where(['product_id' => $id, 'package_id' => $package_id])->first();
        if(!$product_package){
            return response()->json([
                'message' => 'រកមិនឃើញទិន្នន័យ', 
            ], 404);
        }
        $product_package->delete();

        $package = Package::find($package_id);
        if(!$package){
            return response()->json([
                'message' => 'រកមិនឃើញទិន្នន័យ', 
            ], 404);
        }
        $package->delete();

        $data = Main::find($id);
        if(!$data){
            return response()->json([
                'message' => 'រកមិនឃើញទិន្នន័យ', 
            ], 404);
        }
        $data->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'បានលុបចោលដោយជោគជ័យ',
        ], 200);
    }

     function getStats($id = 0){
        //return $id;
        if($id!=0){
            $data = Main::select('id')->withCount(
                [
                    'mPackages as num_of_packages', 

                ])->findOrFail($id);
            if($data){
                return response()->json(['data'=>$data], 200);
            }else{
                return response()->json(['status_code'=>404], 404);
            }
        }
    }
   
}
