<?php

namespace App\Api\V1\Controllers\CP\Package;

use Illuminate\Http\Request;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Package\PackageImage as Main;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

// Import Resource
use App\Api\V1\Resources\Package\PackageResource;
use App\Api\V1\Resources\Package\PackagesCollection;

class PackageImageController extends ApiController
{
    use Helpers;
    function listing($id=0){

        $data = Main::select('id', 'title', 'image')
        ->where( 'package_id', $id)
        ->orderBy( 'id', 'desc')
        ->get();

        $data = new PackagesCollection($data);
        return response()->json($data, 200);
    }

    function view($id = 0){
        
        $data   = Main::select('*')->find($id);

        if($data){
            $data = new PackageResource($data);
            return response()->json(['data' => $data], 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
        
    }

    function create(Request $request, $package_id=0){
        $admin_id = JWTAuth::parseToken()->authenticate()->id;
        
        $data = new Main();

        $data->package_id = $package_id;
        $data->title = $request->input('title');
        
        
        $last = Main::select('id')->orderBy('id', 'DESC')->first();
        $id = 0;
        if($last){
            $id = $last->id+1;
        }


        $image = FileUpload::uploadImage($request, 'image', ['/uploads', '/products', '/'.$id, '/images'], [['xs', 200, 200]]);
        if($image != ""){
            $data->image = $image;
        }
        $image = FileUpload::resize($request, 'image', ['/uploads', '/products', '/'.$id, '/images'], [['xs', 200, 200]]);
        if($image != ""){
            $data->image = $image;
        }
        if($image != ""){
            $data->image = $image;
        }
        $data->save();
        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ', 
            'data' => $data, 
        ], 200);
    }
    
   
    function delete($id=0, $standard_id=0){
        $data = Main::find($standard_id);
        if(!$data){
            return response()->json([
                'message' => 'រកមិនឃើញទិន្នន័យ', 
            ], 404);
        }
        $data->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវបានលុបចោល',
        ], 200);
    }

}
