<?php

namespace App\Api\V1\Controllers\CP\Purchase;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use App\Model\Purchase\Main as Purchase;
use App\Model\User\Main as User;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

class PurchaseController extends ApiController
{
    use Helpers;
    function listing(Request $req) {
       
        $data = Purchase::select('*')
        ->with([
            'details', 
            'member',
            'subscription'
        ]);

        // ==============================>> Keyword
        if( $req->key && $req->key !="" ){
            $data = $data->whereHas('member', function($q) use($req){
                $q->whereHas('user', function($q) use($req){
                    $q->where('name', 'like', $req->key)->orWhere('phone', 'like', $req->key)->orWhere('email', 'like',  $req->key)->orWhere('uid', 'like', $req->key);
                });
            });
        }

        // ==============================>> Purchase ID
        
        if( $req->receipt_number && $req->receipt_number !="" ){
            $data = $data->where('receipt_number', $req->receipt_number);
        }

         // ==============================>> Date Range
         if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        // ==============================>> Purchase ID
        
        if( $req->purchase_id && $req->purchase_id !="" ){
            $data = $data->where('id', $req->purchase_id);
        }

        if($req->member && $req->member != ''){
            $data = $data->where('member_id', $req->member);
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }

}
