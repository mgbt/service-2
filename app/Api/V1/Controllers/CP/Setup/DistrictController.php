<?php

namespace App\Api\V1\Controllers\CP\Setup;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Setup\District as District;
use App\Model\Setup\Country as Country;
use App\Model\Member\Main as Member;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
// use App\Model\User as Model;

class DistrictController extends ApiController
{
    use Helpers;
    function listing(Request $req) {

        $data = District::select('id', 'province_id', 'name')
        ->with([
            'province:id,name',
            'depots'
        ])
        ->withCount([
            // 'districts as n_of_districts',
            'members as n_of_members',
        ]);

        // key search API
        $key       =   isset($_GET['key']) ? $_GET['key'] : "";
        if ($key != "") {
            $data = $data->where(function ($query) use ($key) {
                $query->where('name', 'like', '%' . $key . '%');
            });
        }

        // Sreach by Province
        if($req->province){
            $data = $data->where('province_id', $req->province);
        }

        
        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }

    function view($id = 0){

        $data   = District::select('*')
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
        
    }
    
    function create(Request $req)
    {
       
        $this->validate($req, [
            'name'              => 'required|max:50',
            'province_id'       => 'required|exists:province,id'
            
        ], [
            'name.required'         => 'Please enter discrict name.',
            'name.max'              => 'Discrict name must not be more then 50 chacters.',

            'province_id.required'  => 'Please select a province.', 
            'province_id.exists'    => 'Please select a valid province.',


        ]);

        $data                       = new District();
        $data->name                 = $req->name;
        $data->province_id          = $req->input('province_id');
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }
    
    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'name'              => 'required|max:20',
            'province_id' => 'required|exists:province,id'
        ], [
            'name.required'         => 'Plese enter discrict name.',
            'name.max'              => 'Discrict name must not be more then 20 chacters.',
           
        ]);

        $data   = District::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
            $data->name                 = $req->name;
            $data->province_id          = $req->province_id;

            $data->save();
            
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }

       
    }

    function delete($id = 0)
    {
        $data   = District::find($id); 
        if($data){
            // Start to update
            $data->delete();
            
            return response()->json([
                'status' => 'success',
                'message' => 'បានលុបដោយជោគជ័យ។',
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }

    function provinces(Request $request){
       
        $data           = Province::select('id', 'name')
        ->with([
            'districts:id,province_id,name'
        ])
        ->get();
        return response()->json($data, 200);

    }

}
