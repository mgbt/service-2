<?php

namespace App\Api\V1\Controllers\CP\Setup;

use Illuminate\Http\Request;

use App\Api\V1\Controllers\ApiController;
use App\Model\Setup\Role;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
// use App\Model\User as Model;

class RoleController extends ApiController
{
    use Helpers;
    function listing() {

        $data = Role::select('*')
        ->with([
            'member',
         
        ])
        ->withCount([
            'member as n_of_members',
            // 'subscriptions as n_of_subscriptions',
        ])
        ->get();

        return response()->json($data, 200);
    }

}
