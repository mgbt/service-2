<?php

namespace App\Api\V1\Controllers\CP\Setup;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Subscription\Main as Subscription;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
// use App\Model\User as Model;

// Import Resource
use App\Api\V1\Resources\Subscription\SubscriptionResource;
use App\Api\V1\Resources\Subscription\SubscriptionCollection;

class SubscriptionController extends ApiController
{
    use Helpers;
    function listing(Request $req) {

        $data = Subscription::select('*')
        
        ->withCount([
            'members as n_of_members',
        ])->get(); 
        // return response()->json($data, 200);
        $data = new SubscriptionCollection($data);
        return $data;
    }
    function view($id = 0){

        $data   = Subscription::select('*') ->find($id); 

        if($data){
            $data = new SubscriptionResource($data);
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'Record not found.'
            ], 404);
        }  
    }
    function update(Request $req, $id=0){
        $this->validate($req, [
           
            'icon'             => 'required',
        ], 
        [
            
     
            'icon.required'             => 'Please enter icon.',
 
        ]);


        $data                           = Subscription::find($id);
        $data->name                     = $req->input('name');
      
        //Need to create folder before storing images
        
        $image = FileUpload::uploadImage($req, 'icon', ['/uploads', '/products', '/'.$id, '/images'], [['xs', 200, 200]]);
            if($image != ""){
                $data->icon = $image;
            }
            $image = FileUpload::resize($req, 'icon', ['/uploads', '/products', '/'.$id, '/images'], [['xs', 200, 200]]);
            if($image != ""){
                $data->icon = $image;
            }
            if($image != ""){
                $data->icon = $image;
            }
        
       $data->save();
            
        return response()->json([
            'status' => 'success',
            'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
            'data' => $data,
            
        ], 200);
    }

}
