<?php

namespace App\Api\V1\Controllers\CP\Setup;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Setup\Province as Province;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
// use App\Model\User as Model;

class ProvinceController extends ApiController
{
    use Helpers;
    function listing(Request $req) {

        $data = Province::select('id', 'name', 'country_id', 'created_at')
        ->with([
            // 'country',
            'branches',
        ])
        ->withCount([
            'districts as n_of_districts', 
            'members as n_of_members'
        ])
        ->get();
        return response()->json($data, 200);
    }

    function view($id = 0){

        $data   = Province::select('*')
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
        
    }
    
    function create(Request $req)
    {
       
        $this->validate($req, [
            'name'              => 'required|max:20',
            
        ], [
            'name.required'         => 'Plese enter province name.',
            'name.max'              => 'Province name must not be more then 20 chacters.',

        ]);

        $data                       = new Province();
        $data->name                 = $req->name;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }
    
    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'name'              => 'required|max:20',
           
        ], [
            'name.required'         => 'Plese enter province name.',
            'name.max'              => 'Province name must not be more then 20 chacters.',
           
        ]);

        $data   = Province::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
            $data->name                 = $req->name;
            $data->save();
            
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }

       
    }
    
    function delete($id = 0)
    {
        $data   = Province::find($id); 
        if($data){
            // Start to update
            $data->delete();
            
            return response()->json([
                'status' => 'success',
                'message' => 'បានលុបដោយជោគជ័យ។',
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }

}
