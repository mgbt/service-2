<?php

namespace App\Api\V1\Controllers\CP\Setup;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Setup\Country as Country;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
//use App\Model\User as Model;

class CountryController extends ApiController
{
    use Helpers;
    function listing(Request $req) {

        $data = Country::select('id', 'name', 'abbre', 'phone_rex')->get(); 
        return response()->json($data, 200);
    }

    
}
