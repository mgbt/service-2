<?php

namespace App\Api\V1\Controllers\CP\Transaction;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Api\V1\Controllers\ApiController;
use App\Model\Transaction\USD;
use App\Model\Transaction\USDCategory;
use App\Model\Member\Main as Member;


//========================== Use Mail
use App\MGTB\Wallet; 

class USDController extends ApiController
{
    use Helpers;

    function categories(){
        $data = USDCategory::select('id', 'name')->get(); 
        return $data; 
    }

    function transactions(Request $req){

        $data           = USD::select('id', 'category_id', 'type_id', 'member_id', 'amount', 'balance', 'description', 'created_at')
        ->with([
            'category:id,name',
            'member:id,user_id,subscription_id', 
            'member.user', 
            'member.subscription'
        ]);
        
       
        // ==============================>> Date Range
        if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        $key       =   isset($_GET['key'])?$_GET['key']:"";

        if( $key != "" ){
            $data = $data->where(function($query) use ($key){
                $query->where('id', 'like', '%'.$key.'%');
            });   
        }

        $category      =   intval(isset($_GET['category'])?$_GET['category']:0);
        if($category != 0){
            $data= $data->where('category_id', $category);
        }

        $type      =   intval(isset($_GET['type'])?$_GET['type']:0);
        if($type != 0){
            $data= $data->where('type_id', $type);
        }

        $limit          = intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json($data, 200);
    }

    function clearances(Request $req){
        $data = Member::select('id', 'user_id', 'subscription_id')
        ->with([
            'user', 
            'subscription', 
            'balance',
            'roles'
        ])
        ->whereHas('balance', function($query){
            $query->where('amount', '>', 0); 
        })
       ; 

        if( $req->key && $req->key !="" ){
            $data = $data->whereHas('user', function($q) use($req){
                $q->where('name', 'like', $req->key)->orWhere('phone', 'like', $req->key)->orWhere('email', 'like',  $req->key)->orWhere('uid', 'like', $req->key);
            });
        }

        // ==============================>> Date Range
        if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

       $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return $data; 
    }

    function pay(Request $req){
        $member = Member::select('id')
        ->with([
            'balance'
        ])
      
        ->find($req->member)
       ; 
        if($member){
            if($member->balance->amount > 0){
                $user = JWTAuth::parseToken()->authenticate();
                $trx = Wallet::updateBalance([
                    'categoryId'    => 1, // Withdrawal
                    'typeId'        => 1, // Send
                    'amount'        => $member->balance->amount, 
                    'memberId'      => $member->id, 
                    'description'   => 'Admin withdraw', 
                    'adminId'       => $user->id
                ]); 

                return response()->json([
                    'status' => 'success',
                    'message' => 'You have made withrawal for a member.', 
                    'trx' => $trx
                ], 200);
            }   
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Invalid member.', 
        ], 400);

       
       
      
    }
   
}
