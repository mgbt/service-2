<?php

namespace App\Api\V1\Controllers\CP\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Api\V1\Controllers\ApiController;
use App\Model\Package\PackageProduct as Main;
use App\Model\Package\Main as Package;

use Dingo\Api\Routing\Helpers;
use JWTAuth;


class PackageController extends ApiController
{
    use Helpers;

    function gets(){
    	$key       =   isset($_GET['key'])?$_GET['key']:"";
        $data = Package::select('id', 'en_name', 'created_at')->where('name', 'like', '%'.$key.'%')->limit(50)->get();
        return response()->json($data, 200);
    }

    function getExisting($product_id = 0){
        $data = Main::select('*')->with(['package:id,en_name'])->where('product_id', $product_id)->get();
        return response()->json($data, 200);
    }

    function put(Request $request, $product_id = 0){

        $this->validate($request, [
            'object' => 'required'
        ]);

        $object = json_decode($request->input('object'));

        $data = array();
        for($i = 0; $i<count($object); $i++){
            $data[] = ['product_id'=>$product_id, 'package_id'=>$object[$i], 'created_at'=>now()];
        }

        Main::insert($data);
        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវបានបន្ថែម',
            'object' => $object
        ], 200);
    }

    function delete($id = 0, $product_package_id){
        $data = Main::find($product_package_id);
        if(!$data){
            return response()->json([
                'message' => 'រកមិនឃើញទិន្នន័យ', 
            ], 404);
        }
        $data->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវបានលុបចោល',
        ], 200);
    }



}
