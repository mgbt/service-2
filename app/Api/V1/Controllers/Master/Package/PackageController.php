<?php

namespace App\Api\V1\Controllers\Master\Package;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Api\V1\Controllers\ApiController;

use App\Model\Package\Main as Package;
use App\Model\Stock\Detail as Stock;
use App\Model\Purchase\Detail as MemberPurchase;
// Import Resource
use App\Api\V1\Resources\Package\PackageResource;
use App\Api\V1\Resources\Package\PackagesCollection;

class PackageController extends ApiController
{
    use Helpers;
   
    function listing(Request $req){
       
        $user       = JWTAuth::parseToken()->authenticate();
        $data           = Package::select('id', 'sku','kh_name', 'en_name','image', 'cost_price', 'selling_price as price','discount','is_single_product', 'qty', 'n_of_units', 'updated_at')
        ->with([
           
            'prices',
            'products',
            'memberStock',
            'info',
        
        ])
        ->whereHas('products')
        ->withCount([
            'products as n_of_products',
        ])
        ;

        // Search by Key
        if($req->key && $req->key != "") {
            $data = $data->where(function ($query) use ($req) {
                $query->where('kh_name', 'like', '%' . $req->key . '%')
                ->orWhere('en_name', 'like', '%' . $req->key . '%')
                ;
                });
        }
        // search type

        if($req->type){
            if($req->type == 'package'){
                $data = $data->where('is_single_product', 0); 
            }else if($req->type == 'product'){
                $data = $data->where('is_single_product', 1); 
            }
        }
    
        $data = $data->orderBy('is_single_product', 'asc')->paginate( $req->limit ? $req->limit : 100);
        $data = new PackagesCollection($data);
        return $data;
       
    }

    function stocks(Request $request, $packageId = 0){

        $user       = JWTAuth::parseToken()->authenticate();
        $limit          = intval( $request->limit ? $request->limit : 10); 
        if($request->type == 'purchase'){

            $purchase = MemberPurchase::select('id', 'purchase_id as trx_number', 'purchase_id', 'unit_price', 'qty', 'updated_at')
            ->with([
                'purchase:id,member_id', 
                'purchase.member.user:id,name,phone,email'
            ])
            ->where('package_id', $packageId);  

            if($request->key){
                $order->where('purchase_id', $request->key); 
            }
            
            $purchase= $purchase->orderBy('id', 'desc')->paginate($limit);
            return response()->json($purchase, 200);


        }else if($request->type == 'stock-in'){

           
        }
       
       
    }
   
    
}
