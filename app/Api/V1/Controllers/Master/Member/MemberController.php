<?php

namespace App\Api\V1\Controllers\Master\Member;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use App\Model\User\Main as User;
use App\Model\Member\Main as Member;

use App\Model\Member\provinces; 
use App\Model\Setup\Province;
use App\Model\Setup\Role;

use Dingo\Api\Routing\Helpers;
use JWTAuth;

class MemberController extends ApiController
{
    use Helpers;
    function listing(Request $req) {
        $admin = JWTAuth::parseToken()->authenticate();

      
        $data = Member::select('id', 'user_id', 'subscription_id', 'district_id', 'province_id', 'created_at')
        ->with([
            'user', 
            'subscription',
            'district',
            'province',   
            'activeSubscription'
        ])
        ->withCount([
            'orders as n_of_orders', 
            'purchases as n_of_purchases'
        ]); 
  
        // ==============================>> Location
        if($req->province){
            $data = $data->where('province_id', $req->province); 
        }

        if($req->district){
            $data = $data->where('district_id', $req->district); 
        }

        // ==============================>> Subscription
        if($req->subscription){
            $data = $data->where('subscription_id', $req->subscription); 
        }

        // ==============================>> Subscription
        if($req->role){
            $data = $data->whereHas('subscription', function($query) use ($req){
                $query->where('role_id', $req->role); 
            }); 
        }

        // ==============================>> Keyword
        if( $req->key && $req->key !="" ){
            $data = $data->whereHas('user', function($q) use($req){
                $q->where('name', 'like', $req->key)->orWhere('phone', 'like', $req->key)->orWhere('email', 'like',  $req->key)->orWhere('uid', 'like', $req->key);
            });
        }

        // ==============================>> Date Range
        if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }


    function view($id = 0){

        $data   = Member::select('*')
        ->with([
            'user', 
        ])
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }

        if( $req->order_id && $req->order_id !="" ){
            $data = $data->where('id', $req->order_id);
        }
        
    }

    function create(Request $request){
        $admin = JWTAuth::parseToken()->authenticate();

        $this->validate($request, [
            'name' => 'required|max:150',
            'phone' =>  [
                            'required', 
                            'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                            Rule::unique('user', 'phone')
                        ],
            'email'=>   [
                            'sometimes', 
                            'required', 
                            'email', 
                            'max:100', 
                            Rule::unique('user', 'email')
                        ],
           

            ],[

            'name.required'       =>   'Please enter your name.',
            'name.max'            =>   'Name has been digit  to 60.',

            'phone.required'      =>   'Please enter your phone number.',
            'phone.regex'         =>   'Phone number is required.',
            'phone.unique'        =>   'Phone number already exists.',

            'email.required'      =>   'Please enter your email.',
            'email.email'         =>   'Email is required.',
            'email.max'           =>  'email has been digit  to 100.',
            'email.unique'        =>   'Email already exists.',

            ]);
   
        $user = new User();
        $user->type_id = 2;
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->is_active = 1;
        $user->password = bcrypt(rand(10000000, 999999));
        $user->avatar = '{}';
        $user->creator_id = $admin->id;
        $user->updater_id = $admin->id;
        $user->save();

        $member = new Member();
        $member->user_id = $user->id;
        // $member->creator_id = $admin->id;
        // $member->updater_id = $admin->id;
        $member->save();

        return response()->json([
            'status'    => 'success',
            'message'   => 'បានបង្កើតដោយជោគជ័យ', 
            'member'    => $member,
            'user'      => $user
        ], 200);
    }

    function update(Request $request, $id=0){

     
        $admin     = JWTAuth::parseToken()->authenticate();
        $member    = Member::select('*')->find($id);
       //return $id;
        if($member){

            $user = $member->user; 

            $this->validate($request,[

                'name' => 'required|max:150',
                'phone' =>  [
                                'required', 
                                'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                Rule::unique('user', 'phone')
                            ],
                'email'=>   [
                                'sometimes', 
                                'required', 
                                'email', 
                                'max:100', 
                                Rule::unique('user', 'email')
                            ],
                        ],[
                'name.required'       =>   'Please enter your name.',
                'name.max'            =>   'Name has been digit  to 60.',

                'phone.required'      =>   'Please enter your phone number.',
                'phone.regex'         =>   'Phone number is required.',

                'email.required'      =>   'Please enter your email.',
                'email.email'         =>   'Email is required.',
                'email.max'           =>  'email has been digit  to 100.',

              
            ]);

            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->updater_id = $admin->id;
            $user->save();
        
            return response()->json([
                'status' => 'success',
                'message' => 'ទិន្នន័យត្រូវបានធ្វើបច្ចុប្បន្នភាព។', 
                'member' => $member
            ], 200);

        }else{
            return response()->json([
                'status' => 'fail',
                'message' => 'កំណត់ត្រាអតិថិជនមិនត្រឹមត្រូវ។', 
            
            ], 404);
        }
    }

    function delete($id=0){
        $data = Member::find($id);
        if(!$data){
            return response()->json([
                'message' => 'រកមិនឃើញទិន្នន័យ', 
            ], 404);
        }
        $data->delete();

        $User = User::find($data->user_id)->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវបានលុបចោល។ ',
        ], 200);

    }
    
    function fillter(){

        $provinces = Province::select('id', 'name')
        ->with([
            'districts:id,province_id,name'
        ])
        ->get();
        
        $roles = Role::select('id', 'name')
        ->with([
            'subscriptions:id,role_id,name'
        ])
        ->get();

        return response()->json([
            'provinces' => $provinces,
            'roles' => $roles,
        ], 200);
        
    }

    function updatePassword(Request $request, $id=0){
        $this->validate($request, [
            'password' => 'required|min:6|max:60',
        ]);

        $user = User::find($id); 
        if($user){
           
            $user->password = bcrypt($request->input('password'));
            $user->save();

            return response()->json([
                'status' => 'success',
                'message' => 'ពាក្យសម្ងាត់ត្រូវបានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ។', 
               
            ], 200);
        }else{
            return response()->json([
                'status' => 'fial',
                'message' => 'សមាជិកមិនត្រឹមត្រូវ', 
               
            ], 404);
        }
        
        
    }


}
