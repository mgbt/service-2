<?php

namespace App\Api\V1\Controllers\Master\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Package\Product;
use App\Model\Package\Main as Package;
use App\Model\Package\PackageProduct;
use App\Model\Stock\Detail as Stock;
use App\Model\Event\Request as VenueRequest;
use Dingo\Api\Routing\Helpers;
use JWTAuth;


class Controller extends ApiController
{
    use Helpers;
    function listing(Request $req){
        
        $data = Product::select('*')
        ->with([
           'category',
            'info' 
        ]);

        // key search 
        if($req->key && $req->key != "") {
            $data = $data->where(function ($query) use ($req) {
                $query->where('kh_name', 'like', '%' . $req->key . '%')
                ->orWhere('en_name', 'like', '%' . $req->key . '%')
                ;
            });
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }

    function view($id = 0){

        $data   = Product::select('*')
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
        
    }

    function create(Request $req)
    {
       
        $this->validate($req, [
            'name'              => 'required|max:20',
            'cost_price'        => 'required|numeric',
            'selling_price'     => 'required|numeric'
        ], [
            'name.required'         => 'Plese enter product name.',
            'name.max'              => 'Product name must not be more then 20 chacters.',

            'cost_price.required'   => 'Plese enter cost price.',
            'cost_price.numeric'    => 'Cost price must be number.',

            'selling_price.required'    => 'Plese enter selling price.',
            'selling_price.numeric'     => 'Selling price must be number.'
        ]);

        $data                       = new Product();
        $data->name                 = $req->name;
        $data->cost_price           = $req->input('cost_price');
        $data->selling_price        = $req->input('selling_price');
        $data->description          = $req->input('description');
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }


    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'name'              => 'required|max:20',
            'cost_price'        => 'required|numeric',
            'selling_price'     => 'required|numeric',
            'category'          => 'required|numeric',
        ], [
            'name.required'         => 'Plese enter product name.',
            'name.max'              => 'Product name must not be more then 20 chacters.',

            'cost_price.required'   => 'Plese enter cost price.',
            'cost_price.numeric'    => 'Cost price must be number.',

            'selling_price.required'    => 'Plese enter selling price.',
            'selling_price.numeric'     => 'Selling price must be number.'
        ]);

        $data   = Product::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
            $data->name                 = $req->name;
            $data->category_id          = $req->category;
            $data->cost_price           = $req->input('cost_price');
            $data->selling_price        = $req->input('selling_price');
            $data->description          = $req->input('description');
            $data->save();
            
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }

       
    }
    function stocks(Request $request){
        $user       = JWTAuth::parseToken()->authenticate();
        $data       = Stock::select('id', 'package_id', 'qty')
        ->with([
            'package:id,en_name,selling_price', 
            'package.products'
        ])
        ->get(); 
        return response()->json($data, 200);
 
    }

    function delete($id = 0)
    {
        $data   = Product::find($id); 
        if($data){
            // Start to delete
            $data->delete();
            
            return response()->json([
                'status' => 'success',
                'message' => 'បានលុបដោយជោគជ័យ។',
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
   
}
