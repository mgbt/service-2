<?php

namespace App\Api\V1\Controllers\Master\Stock;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Dingo\Api\Routing\Helpers;
use Carbon\Carbon;
use JWTAuth;

use App\Api\V1\Controllers\ApiController;
use App\Model\Stock\Main as Stock;
use App\Model\Package\Main as Package;
use App\Model\User\Main as User;
use App\MGTB\Stock as MGTBStock;

class StockController extends ApiController
{
    
    use Helpers;
   
    function listing(Request $request){
        $user       = JWTAuth::parseToken()->authenticate();
        $data        = Stock::select('id', 'admin_id', 'requested_at', 'stocked_at', 'total_price')
        ->with([
            'details', 
            'admin'  
        ])
      
        ->where('admin_id', $user->admin->id); 

        $limit          = intval(isset($_GET['limit'])?$_GET['limit']:10); 

          // Key search Date API
        $from = isset($_GET['from']) ? $_GET['from'] : "";
        $to = isset($_GET['to']) ? $_GET['to'] : "";
        if (isValidDate($from)) {
            if (isValidDate($to)) {

                $appends['from'] = $from;
                $appends['to'] = $to;

                $from .= " 00:00:00";
                $to .= " 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }

        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json($data, 200);

       
    }
    
    function create(Request $request){
        $this->validate($request, [
            'cart' => 'required',
           
        ]);

        $user         = JWTAuth::parseToken()->authenticate();
        $cart         = json_decode($request->cart);
      
        if(sizeof($cart)>0){

            $details    = [];
            $total      = 0;

            $stock                  = New Stock; 
            $stock->admin_id        = $user->admin->id;
            $stock->requested_at    = now(); 
            $stock->stocked_at      = now(); 
            $stock->save(); 


            foreach($cart as $packageId => $qty){
                $package = Package::select('cost_price as price')->find($packageId); 
                if($package){
                   
                    $total += $qty*$package->price; 

                    $details[] = [
                        'stock_id'      => $stock->id,
                        'package_id'    => $packageId, 
                        'unit_price'    => $package->price, 
                        'qty'           => $qty
                    ]; 
                }
                
            }

            $stock->details()->insert($details); 

            $stock->total_price = $total; 
            $stock->save(); 

             //Update Stock
             MGTBStock::updateMasterStocks('stock-in',  $stock); 
          
            return response()->json([
                'status' => 'success',
                'message' => 'ស្តុកត្រូវបានបន្ថែម។',
                'order' => $stock, 
                'details' => $details
            ], 200);
              
        }else{
            return response()->json([
                'status' => 'error',
                'message' => "មិនត្រឹមត្រូវ", 
            ], 422);
        }
       
    }

}
