<?php

namespace App\Api\V1\Controllers\Master\Purchase;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use App\Model\Purchase\Main as Purchase;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\MGTB\Stock;
use App\MGTB\Bonus; 
use App\MGTB\Subscription; 

use App\MGTB\Bot\BotPurchase;

class PurchaseController extends ApiController
{
    use Helpers;
    function listing(Request $req) {
        $user       = JWTAuth::parseToken()->authenticate();
        $data = Purchase::select('*')
        ->with([
            'details',
            'member',
            'member.branch',
            'member.depot',
            'activation',
            'subscription',
            'trxs'
        ]);
      
        if( $req->status ) {
            if( $req->status == "pending" ){
                $data = $data->where('approved_at', null)->where('rejected_at', null); 
            }else if( $req->status == "completed-rejected" ){
                $data = $data->where(function($query){
                    $query->where('approved_at', '<>', null)
                    ->orWhere('rejected_at', '<>', null)
                    ; 
                }); 
            } 
        }
        // ==============================>> Purchase ID
         if( $req->receipt_number && $req->receipt_number !="" ){
            $data = $data->where('receipt_number', $req->receipt_number);
        }
        // ==============================>> Keyword
        if( $req->key && $req->key !="" ){
            $data = $data->whereHas('member', function($q) use($req){
                $q->whereHas('user', function($q) use($req){
                    $q->where('name', 'like', $req->key)->orWhere('phone', 'like', $req->key)->orWhere('email', 'like',  $req->key)->orWhere('uid', 'like', $req->key);
                });
            });
        }
        // Key search Date API
        $from = isset($_GET['from']) ? $_GET['from'] : "";
        $to = isset($_GET['to']) ? $_GET['to'] : "";
        if (isValidDate($from)) {
            if (isValidDate($to)) {

                $from .= " 00:00:00";
                $to .= " 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }
        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        
        return response()->json($data, 200);   
    }
    function view($receiptNumber=''){
        $user       = JWTAuth::parseToken()->authenticate();
        $data = Purchase::Select('*')
        ->with([
            'member',
            'member.province',
            'member.district',
            'member.sponsor',
            'member.subscription',
            'member.branch',
            'member.depot',
            'details',
            'subscription'
        ])
        ->where('receipt_number', $receiptNumber)
        ->first();
        if($data){
            return response()->json([
                'data' => $data, 
                'user' => $user
             ], 200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => "Invalid Data", 
            ], 400);
        }
        return response()->json([
            'data' => $data, 
            'user' => $user
         ], 200);
    }
    function action(Request $request){

        $this->validate($request, [
            'purchase' => 'required', 
            'type' => 'required'
        ]);
        
        $user       = JWTAuth::parseToken()->authenticate();
        $purchase   = Purchase::with(['member', 'member.sponsor'])->find($request->purchase); 
       
        if($user->admin){
            if($purchase){
               
                if( is_null($purchase->approved_at) && is_null($purchase->rejected_at) ){

                    $now = now(); 

                    if($request->type == 'accepted'){

                        $checkStock = $this->getComparedStock($purchase); 

                        if($checkStock['hasEnoughStock'] == 1){

                            $purchase->approved_at  =  $now->format('Y-m-d H:i:s');
                    
                            //Update Stock for Member
                            Stock::updateMemberStocks('stock-in', $purchase); 

                            // Check for upgrad subscription
                            Subscription::checkSubscriptionStatus($purchase); 

                            // Calculate Commsion
                            $bonuses = Bonus::purchase($purchase); 
                            $purchase->save(); 

                            //$botRes = BotPurchase::action($purchase, "☺ Succefully Accepted!"); 
                            
                            return response()->json([
                                'status' => 'success',
                                'message' => "ទទួលយកដោយជោគជ័យ។ ", 
                                'bonuses' => $bonuses, 
                                'purchase' => $purchase, 
                               
                            ], 200);
                        }else{
                            return response()->json([
                                'status' => 'success',
                                'message' => " Stock is not enough", 
                               
                            ], 400);
                        }


                    }else{

                        $purchase->rejecter_id = $user->admin->id; 
                        $purchase->rejected_at = $now->format('Y-m-d H:i:s');

                        $purchase->save(); 

                        $botRes = BotPurchase::action($purchase, "😭Rejected!!!"); 

                        return response()->json([
                            'status' => 'success',
                            'message' => "បានបដិសេធដោយជោគជ័យ។ ", 
                            'purchase' => $purchase, 
                            
                        ], 200);
                    
                    }

                

                }else{
                    return response()->json([
                        'status' => 'error',
                        'message' => "Dublicated Auction", 
                    ], 422);
                }

            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => "កំណត់ត្រាមិនត្រឹមត្រូវ", 
                ], 422);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => "Access dinied", 
            ], 401);
        }
        
    }

    public function checkPurchase(Request $request){

        $user       = JWTAuth::parseToken()->authenticate();
        $purchase   = Purchase::where([
            'receipt_number' => $request->receipt_number, 
            //'approved_at' => null
        ])

        ->first(); 
       
        if($user->admin){
            if($purchase){

                return $this->getComparedStock($purchase); 

            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => "កំណត់ត្រាមិនត្រឹមត្រូវ", 
                ], 422);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => "Access dinied", 
            ], 401);
        }
    }

    public function getComparedStock($purchase){
        
        $stocks = []; 
        $details = $purchase->details; 
        $hasEnoughStock = 1; 

       
        foreach($details as $detial){
            
            $stock['package'] = $detial->package; 
            $stock['available_qty'] = $detial->package->qty; 
            $stock['purchased_qty'] = $detial->qty; 
            $stock['unit_price'] = $detial->unit_price; 
            $stock['total_price'] = $detial->qty*$detial->unit_price;

            $stocks[] = $stock; 

            if($detial->package->qty < $detial->qty){
                $hasEnoughStock = 0; 
            } 
        }

        return [
            'purchase' => $purchase, 
            'hasEnoughStock' => $hasEnoughStock, 
            'details' => $stocks
        ]; 
    }

}
