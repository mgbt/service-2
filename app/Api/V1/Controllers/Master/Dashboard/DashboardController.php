<?php

namespace App\Api\V1\Controllers\Master\Dashboard;

use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;

use App\Model\Purchase\Main as Purchase;
use App\Model\Order\Main as Order;
use App\Model\Stock\Main as Stock;
use App\Model\Package\Main as Package;

use App\Model\Setup\Province;

use JWTAuth;
use Carbon\Carbon;

// Import Resource
use App\Api\V1\Resources\Package\PackageResource;
use App\Api\V1\Resources\Package\PackagesCollection;
class DashboardController extends ApiController
{
    use Helpers;

    private $a = 'x'; 
   
    function info(){

        $today      = Carbon::today()->toDateTimeString('Y-m-d 00:00:00'); 
        $now        = Carbon::now()->toDateTimeString('Y-m-d h:i:s');
        
        $orders = Order::select('id', 'total_price', 'total_units')
        ->whereBetween('created_at', [$today, $now])
        ->get();

        $totalOrderPrice = $orders->sum('total_price');
        $totalOrderUnits  = $orders->sum('total_units');

        $penddingPurchases = Purchase::select('id', 'total_price', 'total_units')
        ->where('approved_at',null )
        ->get();

        $totalPenddingPurchasePrice = $penddingPurchases->sum('total_price');
        $totalPenddingPurchaseUnits  = $penddingPurchases->sum('total_units');    

        $purchaseApproved = Purchase::select('id', 'total_price', 'total_units')
        ->whereBetween('approved_at', [$today, $now])
        ->get();

        $totalPurchaseApprovedPrice = $purchaseApproved->sum('total_price');
        $totalPurchaseApprovedUnits  = $purchaseApproved->sum('total_units');
        
        $totalStock = Stock::select('id', 'total_price', 'total_units')
        ->whereBetween('created_at', [$today, $now])
        ->get();

        $totalStockPrice = $totalStock->sum('total_price');
        $totalStockUnits = $totalStock->sum('total_units');


        return response()->json([

            'total_order_price'      =>  $totalOrderPrice,
            'total_order_units'      =>  $totalOrderUnits,
            'total_pendding_pruchases_price'    =>  $totalPenddingPurchasePrice,
            'total_pendding_pruchases_units'    =>  $totalPenddingPurchaseUnits,
            'total_purchase_approved_price'     =>  $totalPurchaseApprovedPrice,
            'total_purchase_approved_units'     =>  $totalPurchaseApprovedUnits,
            'total_stock_price'             => $totalStockPrice,
            'total_stock_units'             => $totalStockUnits

            
        ], 200);
    }


    function countPurchases($memberId, $from){
        $purchases = Purchase::select('id', 'total_price')
        ->where('member_id', $memberId)
        ->where('purchased_at', '>=', $from)
        ->get(); 
        $nOfUnites = 0; 
        $totalPrice = 0; 

        foreach($purchases as $purchase){
            $totalPrice += $purchase->total_price; 
            foreach($purchase->details as $detail){
                $nOfUnites += $detail->qty; 
            }
        }

        return [
            'nOfUnits'      => $nOfUnites, 
            'totalPrice'    => $totalPrice
        ]; 
    }

    function getPurchases(){

        $purchases = Purchase::select('id', 'receipt_number', 'member_id', 'created_at', 'total_price' ,'total_units', 'subscription_id', 'purchased_at')
        ->with([
            'member:id,user_id,subscription_id',
            'member.user:id,uid,name,phone',
            'member.subscription:id,name',
            'subscription'
        ])
        ->where('approved_at', null )
        ->limit(20)
        ->orderBy('id', 'desc')->get();

        return $purchases;
    }

    function getPackages(){ 

      $package   = Package::select('id', 'sku', 'kh_name', 'en_name', 'qty','image')

      ->where('qty', '<', 99) 
      ->limit(20)
      ->orderBy('id', 'desc')->get();
      $package = new PackagesCollection($package);
      return $package;
    }

    function getReport(){

       $today = Carbon::parse('today'); 
       $orders          = ['Order']; 
       $purchases       = ['Purchase']; 
       $labels          = ['Sale']; 

       $dates = []; 
       
        for($i = 1 ; $i <= 7; $i++){
            $start =  Carbon::parse('today')->subDays($i)->format('Y-m-d 00:00:00');
            $end = Carbon::parse('today')->subDays($i)->format('Y-m-d 23:59:59'); 
            
            $totalOrder = Order::select('id', 'buyer_id', 'total_price', 'created_at')
            ->whereBetween('created_at', [$start, $end])
            ->get()
            ->sum('total_price'); 

            $orders[] = $totalOrder; 

            $totalPurchase = Purchase::select('id', 'member_id', 'total_price')
            ->whereBetween('created_at', [$start, $end])
            ->get()
            ->sum('total_price'); 

            $purchases[] = $totalPurchase; 

            $labels[] = Carbon::parse('today')->subDays($i)->format('d-M'); 
            
        }

       // return $dates; 
    
        $rounds = [
            $labels,
            $orders,
            $purchases,
           
        ]; 

        return $rounds; 
    }

    function getProvinceSale(){
        
        $provinces  = []; 
        $orders     = []; 
        $purchases  = []; 

        $provinceData  = Province::select('id', 'name')->get();
        
        foreach($provinceData as $row){
            $provinces[] = $row->name;
            
            $totalOrder = Order::select('id', 'buyer_id', 'total_price', 'created_at')->whereHas('buyer', function($query) use ($row){
                $query->where('province_id', $row->id); 
            })
            ->get()->sum('total_price'); 

            $orders[] = $totalOrder; 

            $totalPurchase = Purchase::select('id', 'member_id', 'total_price')->whereHas('member', function($query) use ($row){
                $query->where('province_id', $row->id); 
            })
            ->get()->sum('total_price'); 

            $purchases[] = $totalPurchase; 
            
        }

        return [
            'label'      => $provinces,
            'purchase'  =>  $purchases, 
            'order'     => $orders

        ]; 
    }
}
