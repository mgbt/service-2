<?php

namespace App\Api\V1\Controllers\Master\Customer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Carbon\Carbon;

use App\Api\V1\Controllers\ApiController;

use App\Model\Order\Main as Order;


class OrderController extends ApiController
{
    use Helpers;
   
    function listing(){
       
        $data           = Order::select('id', 'store_id', 'member_id', 'ordered_at', 'paid_at', 'total_price', 'ordered_at')
        ->selectRaw(' total_price*(discount/100) AS discount_in_usd, (total_price - total_price*(discount/100)) AS final_total')
        ->with([
            'customer',
            'store', 
            'details'
            
        ]);

        $limit          = intval(isset($_GET['limit'])?$_GET['limit']:10); 

        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json($data, 200);

    }


}
