<?php

namespace App\Api\V1\Controllers\Master\Customer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Dingo\Api\Routing\Helpers;
use Carbon\Carbon;

use App\Api\V1\Controllers\ApiController;
use App\Model\Member\Main as Customer;

class CustomerController extends ApiController
{
    use Helpers;
   
    function listing(Request $request){
       
        $data           = Customer::select('id', 'user_id')
        ->with([
           'user:id,name,phone,email,creator_id,created_at', 
           'user.creator',
           'district:id,name'
        ])
        ->withCount([
            'orders as n_of_orders'
        ])
        ;

        
        $limit          = intval(isset($_GET['limit'])?$_GET['limit']:10); 

        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json($data, 200);

       
    }

   
   
}
