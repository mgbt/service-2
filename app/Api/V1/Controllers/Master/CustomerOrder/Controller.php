<?php

namespace App\Api\V1\Controllers\Master\CustomerOrder;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\MGTB\Bot\BotOrder;

use Carbon\Carbon;
use App\MGTB\Stock;
use App\MGTB\Bonus; 

use App\Api\V1\Controllers\ApiController;

use App\Model\Member\Main as Customer;
use App\Model\Member\Stock as MemberStock;
use App\Model\Order\Main as Order;
use App\Model\Package\Main as Package;
use App\Model\Product\Main as Product;



class Controller extends ApiController
{
    use Helpers;
   
    function listing(Request $req){
       
        $data           = Order::select('id', 'receipt_number','approved_at', 'total_units', 'buyer_id', 'member_id', 'ordered_at', 'paid_at', 'total_price')
       ->selectRaw(' total_price*(discount/100) AS discount_in_usd, (total_price - total_price*(discount/100)) AS final_total')
        ->with([
            'buyer', 
            'seller',
            'details'

        ]);
        
        $user       = JWTAuth::parseToken()->authenticate();
        // return $user;
        $data = $data->where('member_id', $user->member->id); 

       // ==============================>> Date Range
       if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }
      // =========================== Search receipt number
        if( $req->receipt_number && $req->receipt_number !="" ){
            $data = $data->where('receipt_number', $req->receipt_number);
        }
        //==========================Transaction ID
        if($req->trx && $req->trx != '' ){
            $data = $data->where('id', $req->trx);
        }
       
        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);

    }

    function create(Request $req){

        $this->validate($req, [
            'cart' => 'required',
            'buyer' => 'required|exists:member,id'
        ]);

        $user         = JWTAuth::parseToken()->authenticate();
        $cart         = json_decode($req->cart);
        $customer     = Customer::find($req->buyer); 
        

        if($user->member && $customer){
            if(sizeof($cart)>0){

                $details    = [];
                $total      = 0;
                $totalPrice = 0; 

                $now = now();

                $order                  =   New Order; 
                $order->member_id       =   $user->member->id; 
                $order->buyer_id        =   $req->buyer; 

                $order->paid_at         =   $now; 
                $order->ordered_at      =   $now; 
                $order->approved_at     =   $now; 
                $order->save(); 

                $totalUnits = 0;
                $hasEnoughStock = true; 
                $stocks = []; 

                foreach($cart as $packageId => $qty){
                    $package = Package::whereHas('products')
                    ->find($packageId)
                    ; 
                    if($package) {
                        $totalUnits += $qty*$package->n_of_units; 
                    }
                }
    
                foreach($cart as $packageId => $qty){

                    $package = Package::whereHas('products')
                    ->find($packageId)
                    ; 
                  
                    if($package){
                        $unitPrice = $this->getPriceByQty($package, $totalUnits); 
                        
                        $stock['packge_id'] = $package->id; 
                        $stock['package'] = $package->name; 
                        $stock['available_qty'] = 0;  
                        $stock['ordered_qty'] = $qty;
                        $stock['unit_price'] = $unitPrice; 
                        $stock['total_price'] = $qty*$unitPrice; 
                        
                        $totalPrice += $qty*$unitPrice; 

                        //Check if this member has enough stock.
                        // $memberStock = MemberStock::where([
                        //     // 'member_id' => $user->member->id, 
                        //     'package_id' => $package->id
                        // ])->first();                      
                        
                        // if($memberStock){
                        //     $stock['available_qty'] = $memberStock->qty; 

                        //     if($memberStock->qty >= $qty){
                                
                        //         $details[] = [
                        //             'order_id'      => $order->id,
                        //             'package_id'    => $packageId, 
                        //             'unit_price'    => $unitPrice, 
                        //             'n_of_units'    => $package->n_of_units, 
                        //             'qty'           => $qty
                        //         ]; 

                               
                        //     }else{
                             
                        //         $hasEnoughStock = false; 
                        //     }
                            
                        // }else{
                            
                        //     $hasEnoughStock = false; 
                        // }
                            if($package->qty >= $qty){
                                
                                $details[] = [
                                    'order_id'      => $order->id,
                                    'package_id'    => $packageId, 
                                    'unit_price'    => $unitPrice, 
                                    'n_of_units'    => $package->n_of_units, 
                                    'qty'           => $qty
                                ]; 

                                $stockIndetails[] = [
                                    'order_id'      => $req->buyer,
                                    'package_id'    => $packageId, 
                                    'unit_price'    => $unitPrice, 
                                    'n_of_units'    => $package->n_of_units, 
                                    'qty'           => $qty
                                ]; 


                            }else{
                                $hasEnoughStock = false; 
                            }
                        

                        $stocks[] = $stock; 

                       
                    }

                    
                }

                if($hasEnoughStock){

                    $order->details()->insert($details); 

                    $order->total_price = $totalPrice; 
                    $order->total_units = $totalUnits; 
                    $order->receipt_number = $this->getReceiptNumber(); 
                    $order->save(); 
                    
                    // Update Stock for Member
                    $dataStockIn = ['member_id' => $req->buyer,'details' => $stockIndetails];
                    //return $dataStockIn['details'];
                    $stock      = Stock::updateMasterStock('stock-out', $order); 
                    $stockIn    = Stock::updateMemberStockIn('stock-in', json_decode(json_encode($dataStockIn)) ); 
                    // $bonuses    = Bonus::order($order); 

                    $botRes = BotOrder::new($order); 

                    return response()->json([
                        'status' => 'success',
                        'message' => 'ការបញ្ជាទិញត្រូវបានដាក់ហើយ។',
                        // 'order' => $order, 
                        // 'details' => $details, 
                        // 'bonuses' => $bonuses,
                        //'bot' =>  $botRes
                    ], 200);

                }else{

                    $order->delete(); 
                    return response()->json([
                        'status' => 'fail',
                        'message' => 'ពុំមាន Stock គ្របគ្រាន់',
                        'total' => $total, 
                        'stocks' => $stocks
                        
                    ], 400);
                }

                
        
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => "មិនត្រឹមត្រូវ", 
                ], 422);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => "ការចូលប្រើមិនត្រឹមត្រូវ", 
            ], 401);
        }
    }
    function getPriceByQty($package, $nOfUnits = 0){

        $price = 0; 

        if($package){
            $prices = $package->prices; 
            foreach($prices as $row){
                if($nOfUnits >= $row->from && $nOfUnits <= $row->to){
                    $price = $row->price; 
                }
            }
        }
        

        return $price; 
    }

    function getReceiptNumber(){
        $number = rand(1000000000, 99999999); 

        $check = Order::where('receipt_number', $number)->first(); 
        if($check){
            return $this->getReceiptNumber(); 
        }

        return $number; 
    }

   

    
}
