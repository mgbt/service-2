<?php

namespace App\MGTB\Bot;

use TelegramBot;
use App\Http\Controllers\Controller;

class BotRegister extends Controller
{
    

    // =========================================================================================>> Register
    public static function newRegister($member, $chanel = "Online", $code = ""){
        if($member){

            $chatID = env('REGISTER_CHANNEL_CHAT_ID'); 

            $res = TelegramBot::sendMessage([
                'chat_id' => $chatID, 
                'text' => '
UID: '.$member->user->uid.' 
Sponsor: '.$member->sponsor->user->uid.' 
Name: '.$member->user->name.'
Phone: '.$member->user->phone.'
Place: '.$member->district->name.' - '.$member->province->name.'
Channel: '.$chanel.'
Verify Cdoe: '.$code.'

',               'parse_mode' => 'HTML'
            ]);

            return $res; 
        }
    }

    public static function registerVerify($member, $code = ""){
        if($member){

            $chatID = env('REGISTER_CHANNEL_CHAT_ID'); 

            $res = TelegramBot::sendMessage([
                'chat_id' => $chatID, 
                'text' => ' <b>Successfully Verify</b>
UID: '.$member->user->uid.' 
Sponsor: '.$member->sponsor->user->uid.' 
Name: '.$member->user->name.'
Phone: '.$member->user->phone.'
Place: '.$member->district->name.' - '.$member->province->name.'
Verify Cdoe: '.$code.'

',               'parse_mode' => 'HTML', 
'reply_to_message' => 14
            ]);

            return $res; 
        }
    }

  
 
}
