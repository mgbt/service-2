<?php

namespace App\Model\Purchase;

use App\Model\Setup\Province;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'purchase';

    public function member(){
        return $this->belongsTo('App\Model\Member\Main', 'member_id')
        //->select('id', 'user_id', 'sponsor_id', 'subscription_id', 'depot_id', 'branch_id', '')
        ->with([
            'sponsor',
            'user:id,uid,name_kh,name,email,phone',
            'province',
            'district'
           
        ]);
    }  
    public function subscription(){
        return $this->belongsTo('App\Model\Subscription\Main', 'subscription_id')
        ->select('id', 'name', 'role','icon')
        ;
    }  

    public function activation(){
        return $this->belongsTo('App\Model\Member\Subscription', 'activation_id')
        ->select('id', 'activated_at', 'expired_at')
        ;
    }  

    public function details(){
        return $this->hasMany('App\Model\Purchase\Detail', 'purchase_id')
        ->select('id', 'purchase_id', 'package_id','unit_price', 'qty', 'n_of_units')
        ->with([
            'package:id,kh_name,en_name,qty,image',
            'product:id,kh_name,en_name'
        ]);
    }

    public function trxs(){
        return $this->hasMany('App\Model\Transaction\USD', 'purchase_id')
        ->select('id', 'purchase_id', 'category_id', 'type_id', 'member_id', 'amount', 'description')
        ->with([
            'category:id,name', 
            'member:id,user_id', 
            'member.user'
        ])
        ;
    }
   
  
    
}
