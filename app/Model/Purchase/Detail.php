<?php

namespace App\Model\Purchase;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detail extends Model
{	
	use SoftDeletes;
    protected $table = 'purchase_details';

 
    public function  purchase(){
        return $this->belongsTo('App\Model\Purchase\Main', 'purchase_id');
    }

    public function  package(){
         return $this->belongsTo('App\Model\Package\Main', 'package_id');
    }
    public function  product(){
        return $this->belongsTo('App\Model\Package\Product', 'product_id');
   }
    

}
