<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{	
    protected $table = 'province';

    public function country(){
        return $this->belongsTo('App\Model\Setup\Country', 'country_id');
    }

    public function branches(){
        return $this->hasMany('App\Model\Member\Main', 'branch_id')
        ->select('id', 'user_id', 'province_id', 'district_id', 'depot_id', 'branch_id', 'subscription_id', 'branch_owner_id', 'is_structure_active')
        ->with([
            'user:id,name,email,phone,uid', 
            'subscription:id,name', 
            //'branch', 
            'district', 
            'province'
        ]);
    }

    public function districts(){
        return $this->hasMany('App\Model\Setup\District', 'province_id');
    }

    public function members(){
        return $this->hasMany('App\Model\Member\Main', 'province_id');
    }

}
