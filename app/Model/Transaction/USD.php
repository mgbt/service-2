<?php

namespace App\Model\Transaction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class USD extends Model
{
   	//use SoftDeletes;
    protected $table = 'usd_transaction';

    public function member(){
        return $this->belongsTo('App\Model\Member\Main', 'member_id');
    }

    public function category(){
        return $this->belongsTo('App\Model\Transaction\USDCategory', 'category_id');
    }

  
   
}
