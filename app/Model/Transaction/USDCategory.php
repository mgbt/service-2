<?php

namespace App\Model\Transaction;
use Illuminate\Database\Eloquent\Model;

class USDCategory extends Model
{
   
    protected $table = 'usd_transactions_category';

    public function type(){
        return $this->belongsTo('App\Model\Transaction\Type', 'type_id');
    }

    public function transactions(){
        return $this->hasMany('App\Model\Transaction\USD', 'category_id');
    }
   
}
