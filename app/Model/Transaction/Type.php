<?php

namespace App\Model\Transaction;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
   	
    protected $table = 'usd_transactions_type';

    public function transactions(){
        return $this->hasMany('App\Model\Transaction\USD', 'type_id');
    }
   
}
