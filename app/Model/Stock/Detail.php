<?php

namespace App\Model\Stock;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detail extends Model
{	
	// use SoftDeletes;
    protected $table = 'stock_details';

 
    public function  stock(){
        return $this->belongsTo('App\Model\Stock\Main', 'stock_id');
    }

    public function  package(){
         return $this->belongsTo('App\Model\Package\Main', 'package_id');
    }
    

}
