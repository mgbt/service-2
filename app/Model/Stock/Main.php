<?php

namespace App\Model\Stock;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'stock';

    public function admin(){
        return $this->belongsTo('App\Model\Admin\Main', 'admin_id')
        ->select('id', 'user_id')
        ->with([
            'user:id,name,email,phone'
        ]);
    }

  
    public function details(){
        return $this->hasMany('App\Model\Stock\Detail', 'stock_id')
        ->select('id', 'stock_id', 'package_id', 'unit_price', 'qty')
        ->with([
            'package:id,kh_name,en_name'
        ])
        ;
    }
    
    
}
