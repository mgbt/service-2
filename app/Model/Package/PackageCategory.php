<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageCategory extends Model
{	
	use SoftDeletes;
    protected $table = 'packages_category';

   	public function  packages(){
         return $this->hasMany('App\Model\Package\Main', 'category_id');
    }
    
}
