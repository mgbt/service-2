<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	//use SoftDeletes;
    protected $table = 'package';

    public function  products(){
        return $this->hasMany('App\Model\Package\PackageProduct', 'package_id')
        ->select('id', 'package_id', 'product_id', 'qty')
        ->with([
            'product:id,category_id,sku,kh_name,en_name,selling_price', 
            'product.category:id,name', 
            'product.prices'
        ]);
    }

    public function  prices(){
        return $this->hasMany('App\Model\Package\PackagePrice', 'package_id')
        ->select('id', 'package_id', 'from', 'to', 'price');
    }

    public function orders(){
        return $this->hasMany('App\Model\Order\Detail', 'package_id');
    }
    public function memberStock(){
        return $this->hasOne('App\Model\Member\Stock', 'package_id');
    }
    
    public function branches(){
        return $this->hasMany('App\Model\Member\BranchPackage', 'package_id');
    }

    public function  info(){
        return $this->hasOne('App\Model\Package\PackageProduct', 'product_id')
        ->select('id', 'package_id', 'product_id', 'qty')
        ->with([
            'product:id,sku,kh_name,en_name,image,category_id', 
            'product.category', 
            'product.prices'
        ]);
    }

    public function  packageProducts(){
        return $this->hasMany('App\Model\Package\PackageProduct', 'package_id')
        ->select('id', 'package_id', 'product_id', 'qty')
        ->with([
            'product:id,sku,kh_name,en_name,image,category_id', 
            'product.category', 
            'product.prices'
        ]);
    }

}
