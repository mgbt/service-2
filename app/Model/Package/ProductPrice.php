<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPrice extends Model
{	
	use SoftDeletes;
    protected $table = 'product_prices';

   
    public function  product(){
        
        return $this->belongsTo('App\Model\Package\Product', 'product_id');
    }

}
