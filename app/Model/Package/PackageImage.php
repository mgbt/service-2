<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageImage extends Model
{	
	use SoftDeletes;
    protected $table = 'package_images';

    public function package(){
        return $this->belongsTo('App\Model\Package', 'package_id');
    }
}
