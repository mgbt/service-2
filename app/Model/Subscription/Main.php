<?php

namespace App\Model\Subscription;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'subscription';
    
    public function roles(){
        return $this->belongsTo('App\Model\Setup\Role', 'role_id');
    }
   
    
    public function members(){
        return $this->hasMany('App\Model\Member\Main', 'subscription_id')
        ->select('id', 'user_id', 'subscription_id')
        ->with([
            'user:id,name,phone'
        ]);
    }

    public function purchases(){
        return $this->hasMany('App\Model\Purchase\Main', 'subscription_id');
    }

    public function totalPurchase(){
        return $this->hasMany('App\Model\Purchase\Main', 'subscription_id')->sum('total_price');
    }

}
