<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'order';

    public function buyer(){
        return $this->belongsTo('App\Model\Member\Main', 'buyer_id')
        ->select('id', 'user_id', 'province_id', 'district_id', 'depot_id', 'branch_id', 'subscription_id', 'branch_owner_id', 'is_structure_active')
        ->with([
            'user:id,name,name_kh,email,phone,uid', 
            'subscription:id,name,role', 
            'depot', 
            'branch', 
            'district', 
            'province'
        ]);
    }
    public function member(){
        return $this->belongsTo('App\Model\Member\Main', 'member_id')
        ->with([
            'user:id,uid,name,email,phone'
        ]);
    }
    
    public function seller(){
        return $this->belongsTo('App\Model\Member\Main', 'member_id')
        ->select('id', 'user_id', 'province_id', 'district_id', 'depot_id', 'branch_id', 'subscription_id', 'branch_owner_id', 'is_structure_active')
        ->with([
            'user:id,name,name_kh,email,phone,uid', 
            'subscription:id,name,role', 
            'depot', 
            'branch', 
            'district', 
            'province'
        ]);
    }

    
    public function details(){
        return $this->hasMany('App\Model\Order\Detail', 'order_id')
        ->select('id', 'order_id', 'package_id', 'unit_price', 'qty', 'n_of_units')
        ->with([
            'package:id,kh_name,en_name'
        ])
        ;
    }

    public function trxs(){
        return $this->hasMany('App\Model\Transaction\USD', 'order_id')
        ->select('id', 'order_id', 'category_id', 'type_id', 'member_id', 'amount', 'description')
        ->with([
            'category:id,name', 
            'member:id,user_id,province_id,district_id', 
            'member.user',
            'member.province',
            'member.district'
        ])
        ;
    }
   
   
  
    
}
