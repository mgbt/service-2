<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detail extends Model
{	
	use SoftDeletes;
    protected $table = 'order_details';

 
    public function  order(){
        return $this->belongsTo('App\Model\Order\Main', 'order_id');
    }

    public function  package(){
         return $this->belongsTo('App\Model\Package\Main', 'package_id');
    }
    

}
