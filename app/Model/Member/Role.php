<?php

namespace App\Model\Member;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
   	//use SoftDeletes;
    protected $table = 'role';

    public function subscriptions(){ // 1-M
        return $this->hasMany('App\Model\Subscripton\Main', 'role_id');
    }
   
}
